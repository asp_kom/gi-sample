RWTexture3D<uint> u_Albedo: register(u0);
RWTexture3D<uint> u_Emittance: register(u1);

#include "GI_Voxelization_include.hlsli"

cbuffer VoxelizationCB: register(b0)
{
    VoxelClipBox voxelization_area;
    float4 render_offset;
    float2 voxel_size; //size, 1.0 / size
    uint   voxel_grid_resolution;
    uint   pad;
};

cbuffer ShadowCB: register(b1)
{
    matrix shadow_map_view_projection_matrix_0;
    matrix shadow_map_view_projection_matrix_1;
    matrix shadow_map_view_projection_matrix_2;
    matrix shadow_map_view_projection_matrix_3;
    uint number_shadow_maps;
    float shadowed_multiplier;
};

Texture2D t_shadowTexture_0: register(t0);
Texture2D t_shadowTexture_1: register(t1);
Texture2D t_shadowTexture_2: register(t2);
Texture2D t_shadowTexture_3: register(t3);
SamplerState shadowSampler: register(s0);

float calculate_shadow_multplier(float3 v, Texture2D texturesArray[4], matrix matricesArray[4])
{
    float shadowMultiplier = shadowed_multiplier;

    bool isShadowed = false;
    for (uint i = 0; i < min(number_shadow_maps, 4); i++)
    {
        shadowMultiplier = 1;
        // Is Shadowed
        float4 vInShadowViewProjSpace = mul(float4(v - render_offset.xyz, 1.0f), matricesArray[i]);

        float2 ndc = vInShadowViewProjSpace.xy;
        float2 shadowMapUV = ndc * 0.5 + 0.5;
        shadowMapUV.y = 1.0 - shadowMapUV.y;

        //In Example we have 3 shadow Cascades in single texture
#ifdef IS_TEST_ENVIRONMENT
#define NUM_HORIZONTAL_SHADOWCASCADES 3.0
        shadowMapUV.x /= NUM_HORIZONTAL_SHADOWCASCADES;
        shadowMapUV.x += (NUM_HORIZONTAL_SHADOWCASCADES - 1) / NUM_HORIZONTAL_SHADOWCASCADES;
#endif
        float readDepth = texturesArray[i].SampleLevel(shadowSampler, shadowMapUV, 0).r;
        isShadowed = readDepth < vInShadowViewProjSpace.z - 0.005;

        if (isShadowed)
        {
            return shadowed_multiplier;
        }
    }
    return shadowMultiplier;
}

uint3 compute_voxel_address(float3 v)
{
    int voxelMaxSize = voxel_grid_resolution;
    int3 voxelAddress = floor(v * voxel_size.y);
    voxelAddress = ((voxelAddress % voxelMaxSize) + voxelMaxSize) % voxelMaxSize;
    return voxelAddress;
}

float3 get_world_space_position(uint3 voxel_idx)
{
    float3 lowestPosibleValue = voxelization_area.voxel_clip_center - voxelization_area.voxel_clip_size * 0.5;
    return lowestPosibleValue + (voxel_idx + 0.5) * voxel_size.x;
}

[numthreads(32,1,1)]
void CSMain(uint3 dtid: SV_DispatchThreadID)
{
    uint3 grid_size;
    u_Emittance.GetDimensions(grid_size.x, grid_size.y, grid_size.z);
    grid_size.x /= 4;

    uint3 voxel_idx;
    voxel_idx.z = dtid.x / (grid_size.x * grid_size.y);
    voxel_idx.y = (dtid.x % (grid_size.x * grid_size.y)) / grid_size.x;
    voxel_idx.x = (dtid.x % (grid_size.x * grid_size.y)) % grid_size.x;

    //Shadows
    Texture2D shadowTexturesArray[4];
    shadowTexturesArray[0] = t_shadowTexture_0;
    shadowTexturesArray[1] = t_shadowTexture_1;
    shadowTexturesArray[2] = t_shadowTexture_2;
    shadowTexturesArray[3] = t_shadowTexture_3;

    matrix shadowMatricesArray[4];
    shadowMatricesArray[0] = shadow_map_view_projection_matrix_0;
    shadowMatricesArray[1] = shadow_map_view_projection_matrix_1;
    shadowMatricesArray[2] = shadow_map_view_projection_matrix_2;
    shadowMatricesArray[3] = shadow_map_view_projection_matrix_3;

    float3 voxelWorldSpace = get_world_space_position(voxel_idx);

    float shadowMultiplier = calculate_shadow_multplier(voxelWorldSpace, shadowTexturesArray, shadowMatricesArray);

    uint3 properVoxelAdress = compute_voxel_address(voxelWorldSpace);

    u_Emittance[properVoxelAdress] = shadowMultiplier * u_Albedo[properVoxelAdress];
    u_Emittance[properVoxelAdress + uint3(grid_size.x, 0, 0)] = shadowMultiplier * u_Albedo[properVoxelAdress + uint3(grid_size.x, 0, 0)];
    u_Emittance[properVoxelAdress + uint3(grid_size.x * 2, 0, 0)] = shadowMultiplier * u_Albedo[properVoxelAdress + uint3(grid_size.x * 2, 0, 0)];
    u_Emittance[properVoxelAdress + uint3(grid_size.x * 3, 0, 0)] = shadowMultiplier <= 0 ? 0 : u_Albedo[properVoxelAdress + uint3(grid_size.x * 3, 0, 0)];
}
