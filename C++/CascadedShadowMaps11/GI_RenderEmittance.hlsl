struct VS_INPUT
{
    uint id: SV_VertexID;
};

struct VS_OUTPUT
{
    float2 vTexcoord                        : TEXCOORD0;
    float4 vPosition                        : SV_POSITION;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VSMain( VS_INPUT Input )
{
    VS_OUTPUT Output;

    Output.vTexcoord = float2((Input.id << 1) & 2, Input.id & 2);
    Output.vPosition = float4(Output.vTexcoord * float2(2.0, -2.0) + float2(-1.0, 1.0), 0.0, 1.0);
    return Output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------

#define MAX_NUMBER_OF_VOXEL_CLIPS 10

struct VoxelClipBox
{
    float4 voxel_clip_size;
    float4 voxel_clip_center;
};

cbuffer VoxelizationCB: register(b0)
{
    VoxelClipBox voxelization_area;
    float4 render_offset;
    float2 voxel_size; //size, 1.0 / size
    uint   voxel_grid_resolution;
    uint   VoxelizationCB_pad;
};

cbuffer CameraCB: register(b1)
{
    float4x4 VP_Matrix;
    float4x4 VP_Matrix_prev;
    float4x4 ProjMatrix;
    float4x4 InvViewMatrix;
    float4x4 InvProjMatrix;
};

Texture3D<uint> u_Emittance: register(t0);

struct PS_OUTPUT
{
    float4 Target0: SV_TARGET0;
};

#include "GI_Visualize_include.hlsli"

struct TRACE_RESULTS
{
    float3 emittance;
};

float4 read_emittance(uint3 pos)
{
    uint3 packing_offset = uint3(voxel_grid_resolution, 0, 0);
    float r = u_Emittance.Load(int4(pos, 0));
    float g = u_Emittance.Load(int4(pos + packing_offset, 0));
    float b = u_Emittance.Load(int4(pos + packing_offset * 2, 0));
    float3 color_result = float3(r, g, b);
    float count = u_Emittance.Load(int4(pos + packing_offset * 3, 0));
    return float4(color_result, count);
}

TRACE_RESULTS trace(float3 clip_center, float3 clip_size, float3 start, float3 dir)
{
    TRACE_RESULTS result = (TRACE_RESULTS)0;
    //find ray-clip_map intersection
    if (!is_in_clipmap(clip_center, clip_size, start))
    {
        float3 intersection = float3(0.0, 0.0, 0.0);
        if (!intersect_clipmap(clip_center, clip_size, start, dir, intersection)) return result;
        start = intersection;
    }

    while (is_in_clipmap(clip_center, clip_size, start))
    {
        float3 voxel_internal_position;
        uint3 voxel_address = compute_voxel_address(start, dir, voxel_internal_position);
        float4 emittance = read_emittance(voxel_address);
        
        if (emittance.w > 0.01)
        {
            result.emittance = emittance.xyz / emittance.w;
            return result;
        }
        start = raymarch(start, dir);
    }

    return result;
}

PS_OUTPUT PSMain(VS_OUTPUT Input)
{
    PS_OUTPUT Output;
    float3 clip_center = floor(voxelization_area.voxel_clip_center * voxel_size.yyy) * voxel_size.xxx;
    float3 clip_size = ceil((voxelization_area.voxel_clip_size * 0.5f) * voxel_size.yyy) * voxel_size.xxx * 2;

    float2 ndc = screen_to_ndc(Input.vTexcoord);
    float3 pos_in_view = ndc_to_view(float3(ndc, 0.0));
    float3 pos_in_world_near = view_to_world(pos_in_view);
    float3 pos_in_world_far = view_to_world(ndc_to_view(float3(ndc, 0.1)));
    float3 world_direction = pos_in_world_far - pos_in_world_near;
    world_direction = normalize(world_direction);
    TRACE_RESULTS trace_results = trace(clip_center, clip_size, pos_in_world_near, world_direction);

    Output.Target0 = float4(trace_results.emittance * (1.0 / 256.0), 1.0);

    return Output;
}