//--------------------------------------------------------------------------------------
// File: CascadedShadowsManger.cpp
//
// This is where the shadows are calcaulted and rendered.
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------


#include "dxut.h"

#include "CascadedShadowsManager.h"
#include "DXUTcamera.h"
#include "SDKMesh.h"
#include "DirectXCollision.h"
#include "SDKmisc.h"
#include "resource.h"
#include "DDSTextureLoader.h"

using namespace DirectX;

static const XMVECTORF32 g_vFLTMAX = { FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX };
static const XMVECTORF32 g_vFLTMIN = { -FLT_MAX, -FLT_MAX, -FLT_MAX, -FLT_MAX };
static const XMVECTORF32 g_vHalfVector = { 0.5f, 0.5f, 0.5f, 0.5f };
static const XMVECTORF32 g_vMultiplySetzwToZero = { 1.0f, 1.0f, 0.0f, 0.0f };
static const XMVECTORF32 g_vZero = { 0.0f, 0.0f, 0.0f, 0.0f };
static const UINT clip_resolution   = 128;       //resolution in pixels
static const float voxel_size       = 0.5f;     //size in world units
static UINT diffuse_width = 0;
static UINT diffuse_height = 0;

UINT g_emittance_mips_count = 1; //will be computed in Init()

//--------------------------------------------------------------------------------------
// Initialize the Manager.  The manager performs all the work of caculating the render 
// paramters of the shadow, creating the D3D resources, rendering the shadow, and rendering
// the actual scene.
//--------------------------------------------------------------------------------------
GIManager::GIManager () 
    : m_CoverageComputeShader( nullptr )
    , m_CleanupComputeShader(nullptr)
    , m_ComputeDirectionalLight(nullptr)
    , m_VoxelizationCB(nullptr)
    , m_VoxelizationUpdateCB(nullptr)
    , m_CoverageTextureXYZ_Pos(nullptr)
    , m_CoverageTextureXYZ_Neg(nullptr)
    , m_EmittanceTexture(nullptr)
    , m_CoverageTextureXYZ_Pos_SRV(nullptr)
    , m_CoverageTextureXYZ_Neg_SRV(nullptr)
    , m_VoxelizationStatusRWBuff_UAV(nullptr)
    , m_VoxelizationCB_Data(nullptr)
    , m_MeshCB_Data(nullptr)
    , m_RenderEmittanceVSShader(nullptr)
    , m_RenderEmittancePSShader(nullptr)
    , m_RenderCoverageVSShader(nullptr)
    , m_RenderCoveragePSShader(nullptr)
    , m_VertexShaderBlob(nullptr)
    , m_PixelShaderBlob(nullptr)
    , m_CameraBuffer(nullptr)
    , m_lightParamatersBuffer(nullptr)
    , m_shadowMapParametersBuffer(nullptr)
    , m_ComputeDiffuseShader(nullptr)
    , m_ComputeDiffuseCB(nullptr)
    , m_DiffuseTexture{ nullptr, nullptr }
    , m_DiffuseTexture_UAV{ nullptr, nullptr }
    , m_DiffuseTexture_SRV{ nullptr, nullptr }
    , m_RenderTextureVSShader(nullptr)
    , m_RenderTexturePSShader(nullptr)
    , m_SamplerPoint(nullptr)
    , m_SamplerLinear(nullptr)
    , m_CurrentDiffuseTexture(0)
    , m_EmittanceMipMapper(nullptr)
    , m_CoverageMipMapper(nullptr)
    , m_EmittanceTextureAll_SRV(nullptr)
    , m_AlbedoTexture_SRV(nullptr)
    , m_DiffuseTAA(nullptr)
    , m_ComputeEmittanceMipsCB(nullptr)
    , m_ComputeCoverageMipsCB(nullptr)
{
}


//--------------------------------------------------------------------------------------
// Call into deallocator.  
//--------------------------------------------------------------------------------------
GIManager::~GIManager() 
{
    SAFE_RELEASE(m_CoverageComputeShader);
    SAFE_RELEASE(m_CleanupComputeShader);
    SAFE_RELEASE(m_ComputeDirectionalLight);
    SAFE_RELEASE(m_EmittanceTexture);
    SAFE_RELEASE(m_VoxelizationCB);
    SAFE_RELEASE(m_VoxelizationUpdateCB);
    SAFE_RELEASE(m_VoxelizationStatusRWBuff);

    SAFE_RELEASE(m_CoverageTextureXYZ_Pos);
    SAFE_RELEASE(m_CoverageTextureXYZ_Neg);
    SAFE_RELEASE(m_CoverageTextureXYZ_Pos_SRV);
    SAFE_RELEASE(m_CoverageTextureXYZ_Neg_SRV);
    SAFE_RELEASE(m_VoxelizationStatusRWBuff_UAV);
    SAFE_RELEASE(m_RenderEmittanceVSShader);
    SAFE_RELEASE(m_RenderEmittancePSShader);
    SAFE_RELEASE(m_RenderCoverageVSShader);
    SAFE_RELEASE(m_RenderCoveragePSShader);
    SAFE_RELEASE(m_VertexShaderBlob);
    SAFE_RELEASE(m_PixelShaderBlob);
    SAFE_RELEASE(m_CameraBuffer);
    SAFE_RELEASE(m_lightParamatersBuffer);
    SAFE_RELEASE(m_shadowMapParametersBuffer);
    SAFE_RELEASE(m_ComputeDiffuseShader);
    SAFE_RELEASE(m_ComputeDiffuseCB);
    SAFE_RELEASE(m_DiffuseTexture[0]);
    SAFE_RELEASE(m_DiffuseTexture[1]);
    SAFE_RELEASE(m_DiffuseTexture_SRV[0]);
    SAFE_RELEASE(m_DiffuseTexture_SRV[1]);
    SAFE_RELEASE(m_DiffuseTexture_UAV[0]);
    SAFE_RELEASE(m_DiffuseTexture_UAV[1]);
    SAFE_RELEASE(m_RenderTextureVSShader);
    SAFE_RELEASE(m_RenderTexturePSShader);
    SAFE_RELEASE(m_SamplerPoint);
    SAFE_RELEASE(m_SamplerLinear);
    SAFE_RELEASE(m_EmittanceMipMapper);
    SAFE_RELEASE(m_CoverageMipMapper);
    SAFE_RELEASE(m_ComputeEmittanceMipsCB);
    SAFE_RELEASE(m_ComputeCoverageMipsCB);
    SAFE_RELEASE(m_EmittanceTextureAll_SRV);
    SAFE_RELEASE(m_AlbedoTexture_SRV);
    SAFE_RELEASE(m_DiffuseTAA);


	SAFE_RELEASE(m_InterriorBboxBuffer);
	SAFE_RELEASE(m_InteriorMappingCubemapVSShader);
	SAFE_RELEASE(m_InteriorMappingCubemapPSShader);


    {
        auto lambda = [](const views_pair& it)
        {
            auto view = it.second;
            SAFE_RELEASE(view);
        };
        std::for_each(m_IndicesVerticesViews.begin(), m_IndicesVerticesViews.end(), lambda);
        m_IndicesVerticesViews.clear();
    }
    {
        auto lambda = [](const meshescb_pair& cb_pair)
        {
            auto buffer = cb_pair.second;
            SAFE_RELEASE(buffer);
        };
        std::for_each(m_MeshesCB.begin(), m_MeshesCB.end(), lambda);
        m_MeshesCB.clear();
    }
    {
        for (size_t i = 0; i < g_emittance_mips_count; ++i)
        {
            SAFE_RELEASE(m_EmittanceTexture_UAV[i]);
            SAFE_RELEASE(m_EmittanceTexture_SRV[i]);
            SAFE_RELEASE(m_CoverageTexture_UAVPos[i]);
            SAFE_RELEASE(m_CoverageTexture_UAVNeg[i]);
            SAFE_RELEASE(m_CoverageTexture_SRVPos[i]);
            SAFE_RELEASE(m_CoverageTexture_SRVNeg[i]);
        }
        m_EmittanceTexture_UAV.clear();
        m_EmittanceTexture_SRV.clear();
        m_CoverageTexture_UAVPos.clear();
        m_CoverageTexture_UAVNeg.clear();
        m_CoverageTexture_SRVPos.clear();
        m_CoverageTexture_SRVNeg.clear();
    }
};


//--------------------------------------------------------------------------------------
// Create the resources, compile shaders, etc.
// The rest of the resources are create in the allocator when the scene changes.
//--------------------------------------------------------------------------------------
HRESULT GIManager::Init ( ID3D11Device* pd3dDevice, CascadeConfig* pCascadeConfig )  
{
    m_pShadowCascadeConfig = pCascadeConfig;

    HRESULT hr = CreateComputeShader(L"GI_Coverage.hlsl", "CSMain", pd3dDevice, &m_CoverageComputeShader);
    assert(SUCCEEDED(hr));
    if (FAILED(hr)) return hr;

    hr = CreateComputeShader(L"GI_Cleanup.hlsl", "CSMain", pd3dDevice, &m_CleanupComputeShader);
    assert(SUCCEEDED(hr));
    if (FAILED(hr)) return hr;

    hr = CreateComputeShader(L"GI_ComputeDiffuse.hlsl", "CSMain", pd3dDevice, &m_ComputeDiffuseShader);
    assert(SUCCEEDED(hr));
    if (FAILED(hr)) return hr;

    hr = CreateComputeShader(L"GI_DirectionalLight.hlsl", "CSMain", pd3dDevice, &m_ComputeDirectionalLight);
    assert(SUCCEEDED(hr));
    if (FAILED(hr)) return hr;

    hr = CreateComputeShader(L"GI_MipMapEmittance.hlsl", "CSMain", pd3dDevice, &m_EmittanceMipMapper);
    assert(SUCCEEDED(hr));
    if (FAILED(hr)) return hr;

    hr = CreateComputeShader(L"GI_MipMapCoverage.hlsl", "CSMain", pd3dDevice, &m_CoverageMipMapper);
    assert(SUCCEEDED(hr));
    if (FAILED(hr)) return hr;

    hr = CreateComputeShader(L"GI_TemporalAA.hlsl", "CSMain", pd3dDevice, &m_DiffuseTAA);
    assert(SUCCEEDED(hr));
    if (FAILED(hr)) return hr;

    hr = CreateVSPSShaders(L"GI_RenderEmittance.hlsl", "VSMain", "PSMain", pd3dDevice, &m_RenderEmittanceVSShader, &m_RenderEmittancePSShader);
    assert(SUCCEEDED(hr));
    if (FAILED(hr)) return hr;

    hr = CreateVSPSShaders(L"GI_RenderTexture.hlsl", "VSMain", "PSMain", pd3dDevice, &m_RenderTextureVSShader, &m_RenderTexturePSShader);
    assert(SUCCEEDED(hr));
    if (FAILED(hr)) return hr;

    hr = CreateVSPSShaders(L"GI_RenderCoverage.hlsl", "VSMain", "PSMain", pd3dDevice, &m_RenderCoverageVSShader, &m_RenderCoveragePSShader);
    assert(SUCCEEDED(hr));
    if (FAILED(hr)) return hr;

	hr = CreateVSPSShaders(L"Interior_Mapping_Cubemap.hlsl", "VSMain", "PSMain", pd3dDevice, &m_InteriorMappingCubemapVSShader, &m_InteriorMappingCubemapPSShader);
	assert(SUCCEEDED(hr));
	if (FAILED(hr)) return hr;

    hr = CreateDDSTextureFromFileEx(pd3dDevice, L"D:\\projects\\gi-sample\\C++\\Media\\WindowCubemap\\sampleCubeMap2.dds", 0, D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, D3D11_RESOURCE_MISC_TEXTURECUBE, false, (ID3D11Resource**)&m_CubeMap_Resource, &m_CubeMap_SRV);
	assert(SUCCEEDED(hr));
	if (FAILED(hr)) return hr;
    

    m_VoxelizationCB        = CreateConstantBuffer(pd3dDevice, sizeof(CB_VOXELIZATION_DATA), "CB_VOXELIZATION_DATA");
    m_VoxelizationUpdateCB  = CreateConstantBuffer(pd3dDevice, sizeof(CB_UPDATE_VOXELIZATION_DATA), "CB_UPDATE_VOXELIZATION_DATA");
    m_CameraBuffer          = CreateConstantBuffer(pd3dDevice, sizeof(CB_CAMERA_DATA), "CB_CAMERA_DATA");
    m_lightParamatersBuffer = CreateConstantBuffer(pd3dDevice, sizeof(CB_LIGHT_PROPERTIES_DATA), "CB_LIGHT_PROPERTIES_DATA");
    m_shadowMapParametersBuffer = CreateConstantBuffer(pd3dDevice, sizeof(CB_SHADOW_MAP_DATA), "CB_SHADOW_MAP_DATA");
    m_ComputeEmittanceMipsCB    = CreateConstantBuffer(pd3dDevice, sizeof(CB_MIPMAP_EMITTANCE), "CB_MIPMAP_EMITTANCE");
    m_ComputeCoverageMipsCB = CreateConstantBuffer(pd3dDevice, sizeof(CB_MIPMAP_COVERAGE), "CB_MIPMAP_COVERAGE");
	m_InterriorBboxBuffer = CreateConstantBuffer(pd3dDevice, sizeof(CB_INTERIOR_BBOX_DATA), "CB_INTERIOR_BBOX_DATA");

    m_VoxelizationStatusRWBuff = CreateRWBuffer(pd3dDevice, 1024 * 4, "VoxelizationStatusRWBuff");

    if (m_VoxelizationCB == nullptr)    return E_FAIL;
    if (m_VoxelizationUpdateCB == nullptr) return E_FAIL;
    if (m_CameraBuffer == nullptr)      return E_FAIL;
    if (m_lightParamatersBuffer == nullptr) return E_FAIL;
    if (m_shadowMapParametersBuffer == nullptr) return E_FAIL;

    g_emittance_mips_count      = floor(log2f(float(clip_resolution)));
    g_emittance_mips_count -= 1;

    m_CoverageTextureXYZ_Pos    = Create3DTexture(pd3dDevice, clip_resolution, clip_resolution, clip_resolution, g_emittance_mips_count, DXGI_FORMAT_R32_UINT, "CoverageTextureXYZ_Pos");
    m_CoverageTextureXYZ_Neg    = Create3DTexture(pd3dDevice, clip_resolution, clip_resolution, clip_resolution, g_emittance_mips_count, DXGI_FORMAT_R32_UINT, "CoverageTextureXYZ_Neg");

    m_AlbedoTexture             = Create3DTexture(pd3dDevice, clip_resolution * 4, clip_resolution, clip_resolution, g_emittance_mips_count, DXGI_FORMAT_R32_UINT, "AlbedoTexture");
    m_EmittanceTexture          = Create3DTexture(pd3dDevice, clip_resolution * 4, clip_resolution, clip_resolution, g_emittance_mips_count, DXGI_FORMAT_R32_UINT ,"EmittanceTexture");

    if (m_CoverageTextureXYZ_Pos    == nullptr) return E_FAIL;
    if (m_CoverageTextureXYZ_Neg    == nullptr) return E_FAIL;
    if (m_EmittanceTexture          == nullptr) return E_FAIL;
    if (m_AlbedoTexture             == nullptr) return E_FAIL;

    m_CoverageTextureXYZ_Pos_SRV    = Create3DTextureSRV(pd3dDevice, m_CoverageTextureXYZ_Pos, 1, 0, "CoverageTextureXYZ_Pos_SRV");
    m_CoverageTextureXYZ_Neg_SRV    = Create3DTextureSRV(pd3dDevice, m_CoverageTextureXYZ_Neg, 1, 0, "CoverageTextureXYZ_Neg_SRV");

    if (m_CoverageTextureXYZ_Pos_SRV    == nullptr) return E_FAIL;
    if (m_CoverageTextureXYZ_Neg_SRV    == nullptr) return E_FAIL;

    m_EmittanceTexture_UAV.resize(g_emittance_mips_count);
    m_EmittanceTexture_SRV.resize(g_emittance_mips_count);
    m_CoverageTexture_UAVPos.resize(g_emittance_mips_count);
    m_CoverageTexture_UAVNeg.resize(g_emittance_mips_count);
    m_CoverageTexture_SRVPos.resize(g_emittance_mips_count);
    m_CoverageTexture_SRVNeg.resize(g_emittance_mips_count);
    for (int i = 0; i < g_emittance_mips_count; ++i)
    {
        char buf[128];
        sprintf(buf, "EmittanceTexture_UAV[%d]", i);
        m_EmittanceTexture_UAV[i]       = Create3DTextureUAV(pd3dDevice, m_EmittanceTexture, DXGI_FORMAT_R32_UINT, i, &buf[0]);
        if (m_EmittanceTexture_UAV[i]   == nullptr) return E_FAIL;

        sprintf(buf, "EmittanceTexture_SRV[%d]", i);
        m_EmittanceTexture_SRV[i]          = Create3DTextureSRV(pd3dDevice, m_EmittanceTexture, 1, i, &buf[0]);
        if (m_EmittanceTexture_SRV[i]          == nullptr) return E_FAIL;

        sprintf(buf, "CoverageTexture_UAVPos[%d]", i);
        m_CoverageTexture_UAVPos[i]        = Create3DTextureUAV(pd3dDevice, m_CoverageTextureXYZ_Pos, DXGI_FORMAT_R32_UINT, i, &buf[0]);
        if (m_CoverageTexture_UAVPos[i] == nullptr) return E_FAIL;

        sprintf(buf, "CoverageTexture_UAVNeg[%d]", i);
        m_CoverageTexture_UAVNeg[i] = Create3DTextureUAV(pd3dDevice, m_CoverageTextureXYZ_Neg, DXGI_FORMAT_R32_UINT, i, &buf[0]);
        if (m_CoverageTexture_UAVNeg[i] == nullptr) return E_FAIL;

        sprintf(buf, "EmittanceTexture_SRV[%d]", i);
        m_CoverageTexture_SRVPos[i] = Create3DTextureSRV(pd3dDevice, m_CoverageTextureXYZ_Pos, 1, i, &buf[0]);
        if (m_CoverageTexture_SRVPos[i] == nullptr) return E_FAIL;

        sprintf(buf, "EmittanceTexture_SRV[%d]", i);
        m_CoverageTexture_SRVNeg[i] = Create3DTextureSRV(pd3dDevice, m_CoverageTextureXYZ_Neg, 1, i, &buf[0]);
        if (m_CoverageTexture_SRVNeg[i] == nullptr) return E_FAIL;
    }

    m_AlbedoTexture_UAV = Create3DTextureUAV(pd3dDevice, m_AlbedoTexture, DXGI_FORMAT_R32_UINT, 0, "m_AlbedoTexture_SRV");
    if (m_AlbedoTexture_UAV == nullptr) return E_FAIL;

    m_EmittanceTextureAll_SRV = Create3DTextureSRV(pd3dDevice, m_EmittanceTexture, g_emittance_mips_count, 0, "EmittanceTextureAll_SRV");
    if (m_EmittanceTextureAll_SRV          == nullptr) return E_FAIL;

    m_AlbedoTexture_SRV = Create3DTextureSRV(pd3dDevice, m_AlbedoTexture, 1, 0, "EmittanceTextureAll_SRV");
    if (m_AlbedoTexture_SRV == nullptr) return E_FAIL;


    m_SamplerPoint = CreateSampler(pd3dDevice, D3D11_FILTER_COMPARISON_MIN_MAG_MIP_POINT, "Sampler Point");
    if (m_SamplerPoint == nullptr) return E_FAIL;


    m_SamplerLinear = CreateSampler(pd3dDevice, D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR, "Sampler Linear");
    if (m_SamplerLinear == nullptr) return E_FAIL;

    return hr;
}


void GIManager::ComputeGI(ID3D11DeviceContext* pd3dDeviceContext, CDXUTSDKMesh* pMesh, ID3D11ShaderResourceView* pNormalMap, ID3D11ShaderResourceView* pDepthSRV, CFirstPersonCamera* pActiveCamera, float temporal_aa_weight, ID3D11ShaderResourceView* shadowCascade, XMMATRIX lightView, XMMATRIX lightProj)
{
    CDXUTPerfEventGenerator renderShadowsForAllCascades(DXUT_PERFEVENTCOLOR, L"GIManager::ComputeGI");
    {
        ComputeCoverageEmittance(pd3dDeviceContext, pMesh);
        ID3D11UnorderedAccessView* nullptr_array[4] = { nullptr,nullptr,nullptr,nullptr };
        ID3D11ShaderResourceView* nullptr_resources[4] = { nullptr,nullptr,nullptr,nullptr };
        pd3dDeviceContext->CSSetUnorderedAccessViews(0, 4, &nullptr_array[0], nullptr);
        pd3dDeviceContext->CSSetShaderResources(0, 4, &nullptr_resources[0]);
        pd3dDeviceContext->CSSetShader(nullptr, nullptr, 0);

        ComputeDirectionalLightInfluence(pd3dDeviceContext, shadowCascade, lightView * lightProj);
        pd3dDeviceContext->CSSetUnorderedAccessViews(0, 4, &nullptr_array[0], nullptr);
        pd3dDeviceContext->CSSetShaderResources(0, 4, &nullptr_resources[0]);
        pd3dDeviceContext->CSSetShader(nullptr, nullptr, 0);

        ComputeCoverageMips(pd3dDeviceContext);
        pd3dDeviceContext->CSSetUnorderedAccessViews(0, 4, &nullptr_array[0], nullptr);
        pd3dDeviceContext->CSSetShaderResources(0, 4, &nullptr_resources[0]);
        pd3dDeviceContext->CSSetShader(nullptr, nullptr, 0);

        ComputeEmittanceMips(pd3dDeviceContext);
        pd3dDeviceContext->CSSetUnorderedAccessViews(0, 4, &nullptr_array[0], nullptr);
        pd3dDeviceContext->CSSetShaderResources(0, 4, &nullptr_resources[0]);
        pd3dDeviceContext->CSSetShader(nullptr, nullptr, 0);

        
        ComputeDiffuse(pd3dDeviceContext, pNormalMap, pDepthSRV, pActiveCamera, temporal_aa_weight);
        pd3dDeviceContext->CSSetUnorderedAccessViews(0, 4, nullptr_array, nullptr);
        pd3dDeviceContext->CSSetShaderResources(0, 4, &nullptr_resources[0]);
        pd3dDeviceContext->CSSetShader(nullptr, nullptr, 0);

        PerformDiffuseTAA(pd3dDeviceContext, pDepthSRV);
        pd3dDeviceContext->CSSetUnorderedAccessViews(0, 4, nullptr_array, nullptr);
        pd3dDeviceContext->CSSetShaderResources(0, 4, &nullptr_resources[0]);
        pd3dDeviceContext->CSSetShader(nullptr, nullptr, 0);
    }

}

void GIManager::ComputeDiffuse(ID3D11DeviceContext* pd3dDeviceContext, ID3D11ShaderResourceView* pNormalSRV, ID3D11ShaderResourceView* pDepthSRV, CFirstPersonCamera* pActiveCamera, float weight_temporal_aa)
{
    m_CurrentDiffuseTexture = (m_CurrentDiffuseTexture + 1) & 1;
    CDXUTPerfEventGenerator renderShadowsForAllCascades(DXUT_PERFEVENTCOLOR, L"GIManager::ComputeDiffuse");

    {
        D3D11_MAPPED_SUBRESOURCE resource;
        pd3dDeviceContext->Map(m_CameraBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);

        XMMATRIX proj = pActiveCamera->GetProjMatrix();
        XMMATRIX view = pActiveCamera->GetViewMatrix();
        XMMATRIX matViewProjection = view * proj;

        static XMMATRIX view_projection_prev;
        CB_CAMERA_DATA* camera_data = (CB_CAMERA_DATA*)resource.pData;
        XMStoreFloat4x4(&camera_data->view_projection, XMMatrixTranspose(matViewProjection));
        XMStoreFloat4x4(&camera_data->view_projection_prev, XMMatrixTranspose(view_projection_prev));
        XMStoreFloat4x4(&camera_data->projection, XMMatrixTranspose(proj));
        XMStoreFloat4x4(&camera_data->view_inv, XMMatrixTranspose(XMMatrixInverse(nullptr, view)));
        XMStoreFloat4x4(&camera_data->proj_inv, XMMatrixTranspose(XMMatrixInverse(nullptr, proj)));

        view_projection_prev = matViewProjection;

        pd3dDeviceContext->Unmap(m_CameraBuffer, 0);
    }

    {
        D3D11_MAPPED_SUBRESOURCE resource;
        pd3dDeviceContext->Map(m_lightParamatersBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);

        CB_LIGHT_PROPERTIES_DATA* light_data = (CB_LIGHT_PROPERTIES_DATA*)resource.pData;
        light_data->light_incoming_direction = XMFLOAT4(0, -1, 0, 0);
        pd3dDeviceContext->Unmap(m_lightParamatersBuffer, 0);
    }

    CheckDiffuseMap(pd3dDeviceContext); //recreate if resolution is changed
    {
        D3D11_MAPPED_SUBRESOURCE resource;
        pd3dDeviceContext->Map(m_ComputeDiffuseCB, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
        CB_COMPUTE_DIFFUSE* data    = (CB_COMPUTE_DIFFUSE*)resource.pData;
        data->DiffuseTexSize        = XMUINT2(diffuse_width, diffuse_height);
        data->DiffuseTexSizeRCP     = XMFLOAT2(1.0 / float(diffuse_width), 1.0 / float(diffuse_height));
        data->DiffuseTexMips        = g_emittance_mips_count;
        data->DiffuseVoxelSize      = voxel_size;
        data->DiffuseReprojectionWeight = weight_temporal_aa;
        pd3dDeviceContext->Unmap(m_ComputeDiffuseCB, 0);
    }

    pd3dDeviceContext->CSSetShader(m_ComputeDiffuseShader, nullptr, 0);
    pd3dDeviceContext->CSSetConstantBuffers(0, 1, &m_ComputeDiffuseCB);
    pd3dDeviceContext->CSSetConstantBuffers(1, 1, &m_lightParamatersBuffer);
    pd3dDeviceContext->CSSetConstantBuffers(2, 1, &m_VoxelizationCB); //already updated in ComputeCoveragEmittance
    pd3dDeviceContext->CSSetConstantBuffers(3, 1, &m_CameraBuffer);
    pd3dDeviceContext->CSSetUnorderedAccessViews(0, 1, &m_DiffuseTexture_UAV[m_CurrentDiffuseTexture], nullptr);
    pd3dDeviceContext->CSSetShaderResources(0, 1, &pNormalSRV);
    pd3dDeviceContext->CSSetShaderResources(1, 1, &pDepthSRV);
    pd3dDeviceContext->CSSetShaderResources(2, 1, &m_EmittanceTextureAll_SRV);
    pd3dDeviceContext->CSSetShaderResources(3, 1, &m_DiffuseTexture_SRV[(m_CurrentDiffuseTexture + 1) & 1]);
    pd3dDeviceContext->CSSetSamplers(0, 1, &m_SamplerLinear);

    UINT groups_x = diffuse_width / 8 + 1;
    UINT groups_y = diffuse_height / 8 + 1;
    pd3dDeviceContext->Dispatch(groups_x, groups_y, 1);
}

void GIManager::PerformDiffuseTAA(ID3D11DeviceContext* pd3dDeviceContext, ID3D11ShaderResourceView* pDepthSRV)
{
    pd3dDeviceContext->CSSetShader(m_DiffuseTAA, nullptr, 0);
    pd3dDeviceContext->CSSetConstantBuffers(0, 1, &m_ComputeDiffuseCB);
    pd3dDeviceContext->CSSetConstantBuffers(1, 1, &m_CameraBuffer);
    pd3dDeviceContext->CSSetShaderResources(0, 1, &m_DiffuseTexture_SRV[m_CurrentDiffuseTexture]);
    pd3dDeviceContext->CSSetShaderResources(1, 1, &pDepthSRV);
    pd3dDeviceContext->CSSetUnorderedAccessViews(0, 1, &m_DiffuseTexture_UAV[(m_CurrentDiffuseTexture + 1) & 1], nullptr);
    
    UINT groups_x = diffuse_width / 8 + 1;
    UINT groups_y = diffuse_height / 8 + 1;
    pd3dDeviceContext->Dispatch(groups_x, groups_y, 1);

    m_CurrentDiffuseTexture = (m_CurrentDiffuseTexture + 1) & 1;
}

void GIManager::ComputeCoverageEmittance(ID3D11DeviceContext* pd3dDeviceContext, CDXUTSDKMesh* pMesh)
{
     {
        float clip_sizef = floor((float)clip_resolution * voxel_size);
        
        bool performVoxelization = false;
        {
            auto oneAxisHotArea = perform_voxelization_movement();
            performVoxelization = oneAxisHotArea.initialized;

            {
                //float clip_sizef = floor((float)clip_resolution * voxel_size);
                D3D11_MAPPED_SUBRESOURCE resource;
                pd3dDeviceContext->Map(m_VoxelizationCB, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
                m_VoxelizationCB_Data = (CB_VOXELIZATION_DATA*)resource.pData;
                m_VoxelizationCB_Data->voxel_size = DirectX::XMFLOAT2(voxel_size, 1.0f / voxel_size);
                m_VoxelizationCB_Data->voxelization_area.size = DirectX::XMFLOAT4(clip_sizef, clip_sizef, clip_sizef, 1.0f / clip_sizef);
                m_VoxelizationCB_Data->voxelization_area.center = DirectX::XMFLOAT4(m_Voxelization_Center_Offset.x, m_Voxelization_Center_Offset.y, m_Voxelization_Center_Offset.z, 0.0f);
                m_VoxelizationCB_Data->payload = DirectX::XMFLOAT4(0, 0, 0, 0);
                m_VoxelizationCB_Data->voxel_grid_resolution = clip_resolution;
                pd3dDeviceContext->Unmap(m_VoxelizationCB, 0);
            }

            BoxCenterSize voxelizationArea;
            voxelizationArea.center = XMFLOAT4(m_Voxelization_Center_Offset.x, m_Voxelization_Center_Offset.y, m_Voxelization_Center_Offset.z,0);
            voxelizationArea.size = XMFLOAT4(clip_sizef, clip_sizef, clip_sizef, 0);

            BoxCenterSize boxes[10];
            boxes[0].center = XMFLOAT4((oneAxisHotArea.min_point.x + oneAxisHotArea.max_point.x) / 2, (oneAxisHotArea.min_point.y + oneAxisHotArea.max_point.y) / 2, (oneAxisHotArea.min_point.z + oneAxisHotArea.max_point.z) / 2, 1.0f);
            boxes[0].size = XMFLOAT4(oneAxisHotArea.max_point.x - oneAxisHotArea.min_point.x, oneAxisHotArea.max_point.y - oneAxisHotArea.min_point.y, oneAxisHotArea.max_point.z - oneAxisHotArea.min_point.z, 1.0f);
            boxes[0].initialized = true;

            BoxCenterSize temp;
            temp.center = XMFLOAT4(0, 0, 0, 0);
            temp.size = XMFLOAT4(10, 10, 10, 0);
            boxes[1] = GetBoxOverlap(temp, voxelizationArea);
   
            {
                D3D11_MAPPED_SUBRESOURCE resource;
                pd3dDeviceContext->Map(m_VoxelizationUpdateCB, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
                m_UpdateVoxelizationCB_Data = (CB_UPDATE_VOXELIZATION_DATA*)resource.pData;

                m_UpdateVoxelizationCB_Data->number_clip_boxes = 0;
                for (int i = 0; i < 10; i++)
                {
                    if (boxes[i].initialized)
                    {
                        m_UpdateVoxelizationCB_Data->voxel_clip_boxes[m_UpdateVoxelizationCB_Data->number_clip_boxes].size = boxes[i].size;
                        m_UpdateVoxelizationCB_Data->voxel_clip_boxes[m_UpdateVoxelizationCB_Data->number_clip_boxes].center = boxes[i].center;
                        m_UpdateVoxelizationCB_Data->number_clip_boxes++;
                    }
                }

                pd3dDeviceContext->Unmap(m_VoxelizationUpdateCB, 0);
            }
        }
     

        if (performVoxelization)
        {
            // Clean VoxelizerArea
            {
                CDXUTPerfEventGenerator renderShadowsForAllCascades(DXUT_PERFEVENTCOLOR, L"Cleanup GI buffers");
                pd3dDeviceContext->CSSetShader(m_CleanupComputeShader, nullptr, 0);
                pd3dDeviceContext->CSSetUnorderedAccessViews(0, 1, &m_CoverageTexture_UAVPos[0], nullptr);
                pd3dDeviceContext->CSSetUnorderedAccessViews(1, 1, &m_CoverageTexture_UAVNeg[0], nullptr);
                pd3dDeviceContext->CSSetUnorderedAccessViews(2, 1, &m_VoxelizationStatusRWBuff_UAV, nullptr);
                pd3dDeviceContext->CSSetUnorderedAccessViews(3, 1, &m_AlbedoTexture_UAV, nullptr);
                pd3dDeviceContext->CSSetConstantBuffers(0, 1, &m_VoxelizationCB);
                pd3dDeviceContext->CSSetConstantBuffers(1, 1, &m_VoxelizationUpdateCB);

                UINT group_count_x = ceil(float(clip_resolution * clip_resolution * clip_resolution) / 32.0f); //32 is a group size in shader
                pd3dDeviceContext->Dispatch(group_count_x, 1, 1);

                ID3D11UnorderedAccessView* nullptr_uavs[4] = { nullptr,nullptr,nullptr,nullptr };
                pd3dDeviceContext->CSSetUnorderedAccessViews(0, 4, &nullptr_uavs[0], nullptr);
            }

            CDXUTPerfEventGenerator renderComputeCoverage(DXUT_PERFEVENTCOLOR, L"Compute coverage and Emittance");
            pd3dDeviceContext->CSSetShader(m_CoverageComputeShader, nullptr, 0);
            pd3dDeviceContext->CSSetUnorderedAccessViews(0, 1, &m_CoverageTexture_UAVPos[0], nullptr);
            pd3dDeviceContext->CSSetUnorderedAccessViews(1, 1, &m_CoverageTexture_UAVNeg[0], nullptr);
            pd3dDeviceContext->CSSetUnorderedAccessViews(2, 1, &m_VoxelizationStatusRWBuff_UAV, nullptr);
            pd3dDeviceContext->CSSetUnorderedAccessViews(3, 1, &m_AlbedoTexture_UAV, nullptr);
            pd3dDeviceContext->CSSetConstantBuffers(0, 1, &m_VoxelizationCB);
            pd3dDeviceContext->CSSetConstantBuffers(2, 1, &m_VoxelizationUpdateCB);

            pd3dDeviceContext->CSSetSamplers(0, 1, &m_SamplerLinear);

            UINT meshes_count = pMesh->GetNumMeshes();
            for (UINT i = 0; i < meshes_count; ++i)
            {
                //check bbox
                BBOX bbox;
                GetBBox(pMesh, i, bbox);

                bool isInAnyCubeBox = false;
                for (int j = 0; j < m_UpdateVoxelizationCB_Data->number_clip_boxes; j++)
                {
                    if (IsInClipmap(bbox, XMFLOAT3(m_UpdateVoxelizationCB_Data->voxel_clip_boxes[j].center.x,
                        m_UpdateVoxelizationCB_Data->voxel_clip_boxes[j].center.y,
                        m_UpdateVoxelizationCB_Data->voxel_clip_boxes[j].center.z),
                        XMFLOAT3(m_UpdateVoxelizationCB_Data->voxel_clip_boxes[j].size.x,
                            m_UpdateVoxelizationCB_Data->voxel_clip_boxes[j].size.y,
                            m_UpdateVoxelizationCB_Data->voxel_clip_boxes[j].size.z))
                        )
                    {
                        isInAnyCubeBox = true;
                        break;
                    }
                }
                if (!isInAnyCubeBox) continue;

                UINT subsets_count = pMesh->GetNumSubsets(i);
                //assert(subsets_count == 1);
                for (UINT j = 0; j < subsets_count; ++j)
                {
                    SDKMESH_SUBSET* pSubset = pMesh->GetSubset(i, j);
                    D3D11_PRIMITIVE_TOPOLOGY topology = CDXUTSDKMesh::GetPrimitiveType11((SDKMESH_PRIMITIVE_TYPE)pSubset->PrimitiveType);


                    UINT vertex_size = pMesh->GetVertexStride(i, 0);
                    ID3D11ShaderResourceView* vertex_view = GetViewForBuffer(pd3dDeviceContext, pMesh->GetVB11(i, 0), pMesh->GetNumVertices(i, 0), vertex_size);
                    ID3D11ShaderResourceView* index_view = GetViewForBuffer(pd3dDeviceContext, pMesh->GetIB11(i), pMesh->GetNumIndices(i), pMesh->GetIndexType(i) == IT_16BIT ? 2 : 4);
                    ID3D11ShaderResourceView* diffuse_view = pMesh->GetMaterial(pSubset->MaterialID)->pDiffuseRV11;

                    D3D11_SHADER_RESOURCE_VIEW_DESC desc = {};
                    diffuse_view->GetDesc(&desc);

                    pd3dDeviceContext->CSSetShaderResources(0, 1, &index_view);
                    pd3dDeviceContext->CSSetShaderResources(1, 1, &vertex_view);
                    pd3dDeviceContext->CSSetShaderResources(2, 1, &diffuse_view);

                    UINT triangles_count = pSubset->IndexCount / 3;
                    //triangles_count = triangles_count < 1024 ? triangles_count : 1024;

                    ID3D11Buffer* meshcb = GetMeshVoxelizationCB(topology, pMesh, i, j, pd3dDeviceContext);
                    pd3dDeviceContext->CSSetConstantBuffers(1, 1, &meshcb);

                    UINT group_count_x = (triangles_count / 32); //32 is a group size in shader
                    pd3dDeviceContext->Dispatch(group_count_x, 1, 1);
                }
            }
        }
    }
}

void GIManager::ComputeDirectionalLightInfluence(ID3D11DeviceContext * pd3dDeviceContext, ID3D11ShaderResourceView * shadowCascade, XMMATRIX lightViewProj)
{
    {
        D3D11_MAPPED_SUBRESOURCE resource;
        pd3dDeviceContext->Map(m_shadowMapParametersBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);

        CB_SHADOW_MAP_DATA* shadowMapData = (CB_SHADOW_MAP_DATA*)resource.pData;
        XMStoreFloat4x4(&shadowMapData->shadow_map_view_projection_matrix, XMMatrixTranspose(lightViewProj));
        shadowMapData->num_shadow_maps = 1;
        shadowMapData->shadowed_multiplier = 0.0;
        pd3dDeviceContext->Unmap(m_shadowMapParametersBuffer, 0);
    }

    {
        CDXUTPerfEventGenerator renderShadowsForAllCascades(DXUT_PERFEVENTCOLOR, L"GI Apply Directional Light");
        pd3dDeviceContext->CSSetShader(m_ComputeDirectionalLight, nullptr, 0);
        pd3dDeviceContext->CSSetUnorderedAccessViews(0, 1, &m_AlbedoTexture_UAV, nullptr);
        pd3dDeviceContext->CSSetUnorderedAccessViews(1, 1, &m_EmittanceTexture_UAV[0], nullptr);
        pd3dDeviceContext->CSSetConstantBuffers(0, 1, &m_VoxelizationCB);
        pd3dDeviceContext->CSSetConstantBuffers(1, 1, &m_shadowMapParametersBuffer);
        pd3dDeviceContext->CSSetShaderResources(0, 1, &shadowCascade);



        UINT group_count_x = ceil(float(clip_resolution * clip_resolution * clip_resolution) / 32.0f); //32 is a group size in shader
        pd3dDeviceContext->Dispatch(group_count_x, 1, 1);

        ID3D11UnorderedAccessView* nullptr_uavs[4] = { nullptr,nullptr,nullptr,nullptr };
        pd3dDeviceContext->CSSetUnorderedAccessViews(0, 4, &nullptr_uavs[0], nullptr);
    }
}

void GIManager::ComputeEmittanceMips(ID3D11DeviceContext* pd3dDeviceContext)
{
    CDXUTPerfEventGenerator renderShadowsForAllCascades(DXUT_PERFEVENTCOLOR, L"GIManager::ComputeEmittanceMips");
    pd3dDeviceContext->CSSetShader(m_EmittanceMipMapper, nullptr, 0);
    for (int i = 1; i < g_emittance_mips_count; ++i)
    {
        {
            //TODO: huge perf penalty here. Do separate buffers, initialize with content and never touch them after that.
            D3D11_MAPPED_SUBRESOURCE resource;
            pd3dDeviceContext->Map(m_ComputeEmittanceMipsCB, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
            CB_MIPMAP_EMITTANCE* emittance_mips = (CB_MIPMAP_EMITTANCE*)resource.pData;
            emittance_mips->EmittanceHiSize = DirectX::XMUINT4(clip_resolution * 4, clip_resolution, clip_resolution, i - 1);
            emittance_mips->EmittanceLoSize = DirectX::XMUINT4((clip_resolution * 4) >> i, clip_resolution >> i, clip_resolution >> i, i);
            emittance_mips->EmittanceHiSizeRcp = DirectX::XMFLOAT4(1.0f / (clip_resolution * 4), 1.0f / clip_resolution, 1.0f / clip_resolution * 4, 1.0f);
            emittance_mips->EmittanceLoSizeRcp = DirectX::XMFLOAT4(1.0f / emittance_mips->EmittanceLoSize.x, 1.0f / emittance_mips->EmittanceLoSize.y, 1.0f / emittance_mips->EmittanceLoSize.z, 1.0f);
            pd3dDeviceContext->Unmap(m_ComputeEmittanceMipsCB, 0);
        }
        pd3dDeviceContext->CSSetConstantBuffers(0, 1, &m_ComputeEmittanceMipsCB);
        pd3dDeviceContext->CSSetUnorderedAccessViews(0, 1, &m_EmittanceTexture_UAV[i], nullptr);
        pd3dDeviceContext->CSSetShaderResources(0, 1, &m_EmittanceTexture_SRV[i-1]);

        UINT groups_count = (clip_resolution >> i)/ 4 + 1; //4 is a group size in compute shader -> [4,4,4]
        pd3dDeviceContext->Dispatch(groups_count, groups_count, groups_count);
    }
}

void GIManager::ComputeCoverageMips(ID3D11DeviceContext* pd3dDeviceContext)
{
    CDXUTPerfEventGenerator renderShadowsForAllCascades(DXUT_PERFEVENTCOLOR, L"GIManager::ComputeCoverageMips");
    pd3dDeviceContext->CSSetShader(m_CoverageMipMapper, nullptr, 0);
    for (int i = 1; i < g_emittance_mips_count; ++i)
    {
        {
            //TODO: huge perf penalty here. Do separate buffers, initialize with content and never touch them after that.
            D3D11_MAPPED_SUBRESOURCE resource;
            pd3dDeviceContext->Map(m_ComputeEmittanceMipsCB, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);
            CB_MIPMAP_COVERAGE* �overage_mips = (CB_MIPMAP_COVERAGE*)resource.pData;
            �overage_mips->CoverageHiSize = DirectX::XMUINT4(clip_resolution * 4, clip_resolution, clip_resolution, i - 1);
            �overage_mips->CoverageLoSize = DirectX::XMUINT4((clip_resolution * 4) >> i, clip_resolution >> i, clip_resolution >> i, i);
            �overage_mips->CoverageHiSizeRcp = DirectX::XMFLOAT4(1.0f / (clip_resolution * 4), 1.0f / clip_resolution, 1.0f / clip_resolution * 4, 1.0f);
            �overage_mips->CoverageLoSizeRcp = DirectX::XMFLOAT4(1.0f / �overage_mips->CoverageLoSize.x, 1.0f / �overage_mips->CoverageLoSize.y, 1.0f / �overage_mips->CoverageLoSize.z, 1.0f);
            pd3dDeviceContext->Unmap(m_ComputeEmittanceMipsCB, 0);
        }
        pd3dDeviceContext->CSSetConstantBuffers(0, 1, &m_ComputeCoverageMipsCB);

        pd3dDeviceContext->CSSetUnorderedAccessViews(0, 1, &m_CoverageTexture_UAVPos[i], nullptr);
        pd3dDeviceContext->CSSetUnorderedAccessViews(1, 1, &m_CoverageTexture_UAVNeg[i], nullptr);

        pd3dDeviceContext->CSSetShaderResources(0, 1, &m_CoverageTexture_SRVPos[i - 1]);
        pd3dDeviceContext->CSSetShaderResources(1, 1, &m_CoverageTexture_SRVNeg[i - 1]);

        UINT groups_count = (clip_resolution >> i) / 4 + 1; //4 is a group size in compute shader -> [4,4,4]
        pd3dDeviceContext->Dispatch(groups_count, groups_count, groups_count);
    }
}

void GIManager::CheckDiffuseMap(ID3D11DeviceContext* pd3dDeviceContext)
{
    if ((diffuse_height != m_pShadowCascadeConfig->m_BackbufferHeight) ||
        (diffuse_width != m_pShadowCascadeConfig->m_BackbufferWidth))
    {
        diffuse_width   = m_pShadowCascadeConfig->m_BackbufferWidth;
        diffuse_height  = m_pShadowCascadeConfig->m_BackbufferHeight;

        CreateDiffuseMap(pd3dDeviceContext);
    }
}

HRESULT GIManager::CreateDiffuseMap(ID3D11DeviceContext* pd3dDeviceContext)
{
    SAFE_RELEASE(m_ComputeDiffuseCB);
    SAFE_RELEASE(m_DiffuseTexture[0]);
    SAFE_RELEASE(m_DiffuseTexture[1]);
    SAFE_RELEASE(m_DiffuseTexture_SRV[0]);
    SAFE_RELEASE(m_DiffuseTexture_SRV[1]);
    SAFE_RELEASE(m_DiffuseTexture_UAV[0]);
    SAFE_RELEASE(m_DiffuseTexture_UAV[1]);

    ID3D11Device* pd3dDevice;
    pd3dDeviceContext->GetDevice(&pd3dDevice);

    m_ComputeDiffuseCB = CreateConstantBuffer(pd3dDevice, sizeof(CB_COMPUTE_DIFFUSE), "CB_COMPUTE_DIFFUSE");
    if (m_ComputeDiffuseCB == nullptr)  return E_FAIL;

    for (int i = 0; i < 2; ++i)
    {
        m_DiffuseTexture[i] = Create2DTexture(pd3dDevice, diffuse_width, diffuse_height, DXGI_FORMAT_R8G8B8A8_TYPELESS, "GI_DiffuseTexture");
        if (m_DiffuseTexture[i] == nullptr)  return E_FAIL;

        m_DiffuseTexture_UAV[i] = Create2DTextureUAV(pd3dDevice, m_DiffuseTexture[i], DXGI_FORMAT_R32_UINT, "GI_DiffuseTexture_UAV");
        if (m_DiffuseTexture_UAV[i] == nullptr) return E_FAIL;

        m_DiffuseTexture_SRV[i] = Create2DTextureSRV(pd3dDevice, m_DiffuseTexture[i], DXGI_FORMAT_R8G8B8A8_UNORM, "GI_DiffuseTexture_SRV");
        if (m_DiffuseTexture_SRV[i] == nullptr) return E_FAIL;
    }

    return S_OK;
}

BoxCenterSize GIManager::GetBoxOverlap(BoxCenterSize box1, BoxCenterSize box2)
{
    BoxCenterSize toReturn;

    XMVECTOR box1Min = XMLoadFloat4(&box1.center) - XMLoadFloat4(&box1.size) / 2;
    XMVECTOR box1Max = XMLoadFloat4(&box1.center) + XMLoadFloat4(&box1.size) / 2;

    XMVECTOR box2Min = XMLoadFloat4(&box2.center) - XMLoadFloat4(&box2.size) / 2;
    XMVECTOR box2Max = XMLoadFloat4(&box2.center) + XMLoadFloat4(&box2.size) / 2;

    XMVECTOR newBoxMin = XMVectorMax(box1Min, box2Min);
    XMVECTOR newBoxMax = XMVectorMin(box1Max, box2Max);

    XMStoreFloat4(&toReturn.center, (newBoxMax + newBoxMin) / 2);
    XMStoreFloat4(&toReturn.size, newBoxMax - newBoxMin);
    toReturn.initialized = (toReturn.size.x > 0 && toReturn.size.y > 0 && toReturn.size.z > 0);

    return toReturn;
}

void GIManager::SlideVoxelization(DirectX::XMFLOAT3 offset)
{
    m_Voxelization_Center_Desired = XMFLOAT3(m_Voxelization_Center_Offset.x + offset.x, m_Voxelization_Center_Offset.y + offset.y, m_Voxelization_Center_Offset.z + offset.z);
}

void GIManager::SlideInteriorBBox(DirectX::XMFLOAT3 offset)
{
	m_InterriorBboxCenter = XMFLOAT4(m_InterriorBboxCenter.x + offset.x, m_InterriorBboxCenter.y + offset.y, m_InterriorBboxCenter.z + offset.z, 1);
}

void GIManager::ScaleInteriorBBox(DirectX::XMFLOAT3 offset)
{
	m_InterriorBboxExtent = XMFLOAT3(m_InterriorBboxExtent.x * offset.x, m_InterriorBboxExtent.y * offset.y, m_InterriorBboxExtent.z * offset.z);
}

void GIManager::RotateInteriorBBox(DirectX::XMFLOAT3 offset)
{
	m_InterriorBboxRotation = XMFLOAT3(m_InterriorBboxRotation.x + offset.x, m_InterriorBboxRotation.y + offset.y, m_InterriorBboxRotation.z + offset.z);
}

BBOX GIManager::perform_voxelization_movement()
{
    BBOX area;
    area.initialized = false;
    area.min_point = XMFLOAT3(0, 0, 0);
    area.max_point = XMFLOAT3(0, 0, 0);

    float size = floor((float)clip_resolution * voxel_size) / 2;
    float step_size = 0;// size * 0.25f;
    float recalculate_whole_size = size * 0.75f;
    XMFLOAT3 min_point = XMFLOAT3(m_Voxelization_Center_Offset.x - size, m_Voxelization_Center_Offset.y - size, m_Voxelization_Center_Offset.z - size);
    XMFLOAT3 max_point = XMFLOAT3(m_Voxelization_Center_Offset.x + size, m_Voxelization_Center_Offset.y + size, m_Voxelization_Center_Offset.z + size);

    float x_dist = m_Voxelization_Center_Desired.x - m_Voxelization_Center_Offset.x;
    float y_dist = m_Voxelization_Center_Desired.y - m_Voxelization_Center_Offset.y;
    float z_dist = m_Voxelization_Center_Desired.z - m_Voxelization_Center_Offset.z;

    // Perform teleport
    if (fabs(x_dist) > recalculate_whole_size || fabs(y_dist) > recalculate_whole_size || fabs(z_dist) > recalculate_whole_size)
    {
        area.initialized = true;
        m_Voxelization_Center_Offset = m_Voxelization_Center_Desired;
        area.min_point = XMFLOAT3(m_Voxelization_Center_Offset.x - size, m_Voxelization_Center_Offset.y - size, m_Voxelization_Center_Offset.z - size);
        area.max_point = XMFLOAT3(m_Voxelization_Center_Offset.x + size, m_Voxelization_Center_Offset.y + size, m_Voxelization_Center_Offset.z + size);
        return area;
    }

    // Perform move X
    if (fabsf(x_dist) > step_size)
    {
        m_Voxelization_Center_Offset.x = m_Voxelization_Center_Desired.x;
        if (x_dist > 0)
        {
            area.min_point = XMFLOAT3(max_point.x, min_point.y, min_point.z);
            area.max_point = XMFLOAT3(max_point.x + x_dist, max_point.y, max_point.z);
        }
        else
        {
            area.min_point = XMFLOAT3(min_point.x + x_dist, min_point.y, min_point.z);
            area.max_point = XMFLOAT3(min_point.x, max_point.y, max_point.z);
        }
        area.initialized = true;
        return area;
    }

    // Perform move Y
    if (fabsf(y_dist) > step_size)
    {
        m_Voxelization_Center_Offset.y  = m_Voxelization_Center_Desired.y;

        if (y_dist > 0)
        {
            area.min_point = XMFLOAT3(min_point.x, max_point.y, min_point.z);
            area.max_point = XMFLOAT3(max_point.x, max_point.y + y_dist, max_point.z);
        }
        else
        {
            area.min_point = XMFLOAT3(min_point.x, min_point.y + y_dist, min_point.z);
            area.max_point = XMFLOAT3(max_point.x, min_point.y, max_point.z);
        }
        area.initialized = true;
        return area;
    }

    // Perform move Z
    if (fabsf(z_dist) > step_size)
    {
        m_Voxelization_Center_Offset.z = m_Voxelization_Center_Desired.z;

        if (z_dist > 0)
        {
            area.min_point = XMFLOAT3(min_point.x, min_point.y, max_point.z);
            area.max_point = XMFLOAT3(max_point.x, max_point.y, max_point.z + z_dist);
        }
        else
        {
            area.min_point = XMFLOAT3(min_point.x, min_point.y, min_point.z + z_dist);
            area.max_point = XMFLOAT3(max_point.x, max_point.y, min_point.z);
        }
        area.initialized = true;
        return area;
    }

    return area;
}


void GIManager::RenderAlbedo(ID3D11DeviceContext * pd3dDeviceContext, CFirstPersonCamera * pActiveCamera, ID3D11RenderTargetView * prtvBackBuffer, ID3D11DepthStencilView * pdsvBackBuffer)
{
    CDXUTPerfEventGenerator renderShadowsForAllCascades(DXUT_PERFEVENTCOLOR, L"GIManager::RenderEmittance");
    FLOAT clearColor[4] = { 0.0, 0.0, 0.0, 0.0 };
    pd3dDeviceContext->ClearRenderTargetView(prtvBackBuffer, clearColor);
    pd3dDeviceContext->ClearDepthStencilView(pdsvBackBuffer, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0, 0);

    pd3dDeviceContext->VSSetShader(m_RenderEmittanceVSShader, nullptr, 0);
    pd3dDeviceContext->IASetInputLayout(nullptr);
    pd3dDeviceContext->PSSetShader(m_RenderEmittancePSShader, nullptr, 0);

    {
        D3D11_MAPPED_SUBRESOURCE resource;
        pd3dDeviceContext->Map(m_CameraBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);

        XMMATRIX proj = pActiveCamera->GetProjMatrix();
        XMMATRIX view = pActiveCamera->GetViewMatrix();
        XMMATRIX matViewProjection = view * proj;

        static XMMATRIX view_projection_prev;

        CB_CAMERA_DATA* camera_data = (CB_CAMERA_DATA*)resource.pData;
        XMStoreFloat4x4(&camera_data->view_projection, XMMatrixTranspose(matViewProjection));
        XMStoreFloat4x4(&camera_data->view_projection_prev, XMMatrixTranspose(view_projection_prev));
        XMStoreFloat4x4(&camera_data->projection, XMMatrixTranspose(proj));
        XMStoreFloat4x4(&camera_data->view_inv, XMMatrixTranspose(XMMatrixInverse(nullptr, view)));
        XMStoreFloat4x4(&camera_data->proj_inv, XMMatrixTranspose(XMMatrixInverse(nullptr, proj)));

        view_projection_prev = matViewProjection;

        pd3dDeviceContext->Unmap(m_CameraBuffer, 0);
    }
    pd3dDeviceContext->PSSetConstantBuffers(0, 1, &m_VoxelizationCB);
    pd3dDeviceContext->PSSetConstantBuffers(1, 1, &m_CameraBuffer);
    pd3dDeviceContext->PSSetShaderResources(0, 1, &m_AlbedoTexture_SRV);

    pd3dDeviceContext->Draw(3, 0);
}

void GIManager::RenderEmittance(ID3D11DeviceContext* pd3dDeviceContext, CFirstPersonCamera* pActiveCamera, ID3D11RenderTargetView* prtvBackBuffer, ID3D11DepthStencilView* pdsvBackBuffer)
{
    CDXUTPerfEventGenerator renderShadowsForAllCascades(DXUT_PERFEVENTCOLOR, L"GIManager::RenderEmittance");
    FLOAT clearColor[4] = { 0.0, 0.0, 0.0, 0.0 };
    pd3dDeviceContext->ClearRenderTargetView(prtvBackBuffer, clearColor);
    pd3dDeviceContext->ClearDepthStencilView(pdsvBackBuffer, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0, 0);

    pd3dDeviceContext->VSSetShader(m_RenderEmittanceVSShader, nullptr, 0);
    pd3dDeviceContext->IASetInputLayout(nullptr);
    pd3dDeviceContext->PSSetShader(m_RenderEmittancePSShader, nullptr, 0);

    {
        D3D11_MAPPED_SUBRESOURCE resource;
        pd3dDeviceContext->Map(m_CameraBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);

        XMMATRIX proj = pActiveCamera->GetProjMatrix();
        XMMATRIX view = pActiveCamera->GetViewMatrix();
        XMMATRIX matViewProjection = view * proj;

        static XMMATRIX view_projection_prev;

        CB_CAMERA_DATA* camera_data = (CB_CAMERA_DATA*)resource.pData;
        XMStoreFloat4x4(&camera_data->view_projection, XMMatrixTranspose(matViewProjection));
        XMStoreFloat4x4(&camera_data->view_projection_prev, XMMatrixTranspose(view_projection_prev));
        XMStoreFloat4x4(&camera_data->projection, XMMatrixTranspose(proj));
        XMStoreFloat4x4(&camera_data->view_inv, XMMatrixTranspose(XMMatrixInverse(nullptr, view)));
        XMStoreFloat4x4(&camera_data->proj_inv, XMMatrixTranspose(XMMatrixInverse(nullptr, proj)));

        view_projection_prev = matViewProjection;

        pd3dDeviceContext->Unmap(m_CameraBuffer, 0);
    }
    pd3dDeviceContext->PSSetConstantBuffers(0, 1, &m_VoxelizationCB);
    pd3dDeviceContext->PSSetConstantBuffers(1, 1, &m_CameraBuffer);
    pd3dDeviceContext->PSSetShaderResources(0, 1, &m_EmittanceTextureAll_SRV);

    pd3dDeviceContext->Draw(3, 0);
}

void GIManager::RenderCoverage(ID3D11DeviceContext* pd3dDeviceContext, CFirstPersonCamera* pActiveCamera, ID3D11RenderTargetView* prtvBackBuffer, ID3D11DepthStencilView* pdsvBackBuffer)
{
    CDXUTPerfEventGenerator renderShadowsForAllCascades(DXUT_PERFEVENTCOLOR, L"GIManager::RenderCoverage");
    FLOAT clearColor[4] = { 0.0, 0.0, 0.0, 0.0 };
    pd3dDeviceContext->ClearRenderTargetView(prtvBackBuffer, clearColor);
    pd3dDeviceContext->ClearDepthStencilView(pdsvBackBuffer, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0, 0);

    pd3dDeviceContext->VSSetShader(m_RenderCoverageVSShader, nullptr, 0);
    pd3dDeviceContext->IASetInputLayout(nullptr);
    pd3dDeviceContext->PSSetShader(m_RenderCoveragePSShader, nullptr, 0);

    {
        D3D11_MAPPED_SUBRESOURCE resource;
        pd3dDeviceContext->Map(m_CameraBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);

        XMMATRIX proj = pActiveCamera->GetProjMatrix();
        XMMATRIX view = pActiveCamera->GetViewMatrix();
        XMMATRIX matViewProjection = view * proj;

        static XMMATRIX view_projection_prev;

        CB_CAMERA_DATA* camera_data = (CB_CAMERA_DATA*)resource.pData;
        XMStoreFloat4x4(&camera_data->view_projection, XMMatrixTranspose(matViewProjection));
        XMStoreFloat4x4(&camera_data->view_projection_prev, XMMatrixTranspose(view_projection_prev));
        XMStoreFloat4x4(&camera_data->projection, XMMatrixTranspose(proj));
        XMStoreFloat4x4(&camera_data->view_inv, XMMatrixTranspose(XMMatrixInverse(nullptr, view)));
        XMStoreFloat4x4(&camera_data->proj_inv, XMMatrixTranspose(XMMatrixInverse(nullptr, proj)));

        view_projection_prev = matViewProjection;

        pd3dDeviceContext->Unmap(m_CameraBuffer, 0);
    }
    pd3dDeviceContext->PSSetConstantBuffers(0, 1, &m_VoxelizationCB);
    pd3dDeviceContext->PSSetConstantBuffers(1, 1, &m_CameraBuffer);
    pd3dDeviceContext->PSSetShaderResources(0, 1, &m_CoverageTextureXYZ_Pos_SRV);
    pd3dDeviceContext->PSSetShaderResources(1, 1, &m_CoverageTextureXYZ_Neg_SRV);

    pd3dDeviceContext->Draw(3, 0);


    ID3D11ShaderResourceView* nullptr_views[2] = { nullptr, nullptr };
    pd3dDeviceContext->PSSetShaderResources(0, 2, &nullptr_views[0]);
}

void GIManager::RenderDiffuse(ID3D11DeviceContext* pd3dDeviceContext, CFirstPersonCamera* /*pActiveCamera*/, ID3D11RenderTargetView* prtvBackBuffer, ID3D11DepthStencilView* pdsvBackBuffer)
{
    CDXUTPerfEventGenerator renderShadowsForAllCascades(DXUT_PERFEVENTCOLOR, L"GIManager::RenderDiffuse");
    FLOAT clearColor[4] = { 0.0, 0.0, 0.0, 0.0 };
    pd3dDeviceContext->ClearRenderTargetView(prtvBackBuffer, clearColor);
    pd3dDeviceContext->ClearDepthStencilView(pdsvBackBuffer, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0, 0);

    pd3dDeviceContext->VSSetShader(m_RenderTextureVSShader, nullptr, 0);
    pd3dDeviceContext->IASetInputLayout(nullptr);
    pd3dDeviceContext->PSSetShader(m_RenderTexturePSShader, nullptr, 0);

    pd3dDeviceContext->PSSetShaderResources(0, 1, &m_DiffuseTexture_SRV[m_CurrentDiffuseTexture]);

    pd3dDeviceContext->PSSetSamplers(0, 1, &m_SamplerPoint);
    pd3dDeviceContext->Draw(3, 0);
}

void GIManager::RenderInteriorWindow(ID3D11DeviceContext * pd3dDeviceContext, ID3D11RenderTargetView * prtvBackBuffer, ID3D11DepthStencilView * pdsvBackBuffer, CDXUTSDKMesh * pMesh, CFirstPersonCamera * pActiveCamera)
{
	pMesh->TransformMesh(
		XMMatrixMultiply( 
			XMMatrixRotationRollPitchYaw(XMConvertToRadians(0), XMConvertToRadians(0), XMConvertToRadians(0)),
			XMMatrixScaling(20, 20, 20))
		, 0);
	{
		D3D11_MAPPED_SUBRESOURCE resource;
		pd3dDeviceContext->Map(m_InterriorBboxBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);

		CB_INTERIOR_BBOX_DATA* bbox_data = (CB_INTERIOR_BBOX_DATA*)resource.pData;
		XMStoreFloat4x4(&bbox_data->model_projection, XMMatrixTranspose(pMesh->GetWorldMatrix(0)));
		XMStoreFloat4x4(&bbox_data->model_projection_inv, XMMatrixTranspose(XMMatrixInverse(nullptr, pMesh->GetWorldMatrix(0))));
		XMStoreFloat4x4(&bbox_data->BBoxRotation, XMMatrixTranspose(XMMatrixRotationRollPitchYawFromVector(XMLoadFloat3(&m_InterriorBboxRotation))));
		bbox_data->position = m_InterriorBboxCenter;
		bbox_data->extent = m_InterriorBboxExtent;
		pd3dDeviceContext->Unmap(m_InterriorBboxBuffer, 0);
	}

	{
		D3D11_MAPPED_SUBRESOURCE resource;
		pd3dDeviceContext->Map(m_CameraBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);

		XMMATRIX proj = pActiveCamera->GetProjMatrix();
		XMMATRIX view = pActiveCamera->GetViewMatrix();
		XMMATRIX matViewProjection = view * proj;

		static XMMATRIX view_projection_prev;
		CB_CAMERA_DATA* camera_data = (CB_CAMERA_DATA*)resource.pData;
		XMStoreFloat4x4(&camera_data->view_projection, XMMatrixTranspose(matViewProjection));
		XMStoreFloat4x4(&camera_data->view_projection_prev, XMMatrixTranspose(view_projection_prev));
		XMStoreFloat4x4(&camera_data->projection, XMMatrixTranspose(proj));
		XMStoreFloat4x4(&camera_data->view_inv, XMMatrixTranspose(XMMatrixInverse(nullptr, view)));
		XMStoreFloat4x4(&camera_data->proj_inv, XMMatrixTranspose(XMMatrixInverse(nullptr, proj)));

		view_projection_prev = matViewProjection;

		pd3dDeviceContext->Unmap(m_CameraBuffer, 0);
	}

	pd3dDeviceContext->VSSetShader(m_InteriorMappingCubemapVSShader, nullptr, 0);
	pd3dDeviceContext->PSSetShader(m_InteriorMappingCubemapPSShader, nullptr, 0);
	pd3dDeviceContext->VSSetConstantBuffers(0, 1, &m_CameraBuffer);
	pd3dDeviceContext->VSSetConstantBuffers(1, 1, &m_InterriorBboxBuffer);
	pd3dDeviceContext->PSSetConstantBuffers(0, 1, &m_CameraBuffer);
	pd3dDeviceContext->PSSetConstantBuffers(1, 1, &m_InterriorBboxBuffer);
	pd3dDeviceContext->PSSetShaderResources(0, 1, &m_CubeMap_SRV);
	pd3dDeviceContext->PSSetSamplers(0, 1, &m_SamplerPoint);

	pMesh->Render(pd3dDeviceContext);
}

ID3D11Buffer* GIManager::CreateConstantBuffer(ID3D11Device* pDevice, UINT byteWidth, const char* debugName)
{
    assert(byteWidth % 16 == 0);
    ID3D11Buffer* result = nullptr;

    D3D11_BUFFER_DESC Desc;
    Desc.Usage          = D3D11_USAGE_DYNAMIC;
    Desc.BindFlags      = D3D11_BIND_CONSTANT_BUFFER;
    Desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    Desc.MiscFlags      = 0;

    Desc.ByteWidth = byteWidth;
    pDevice->CreateBuffer(&Desc, nullptr, &result);
    DXUT_SetDebugName(result, debugName);

    return result;
}

ID3D11Buffer* GIManager::CreateRWBuffer(ID3D11Device* pDevice, UINT byteWidth, const char* debugName)
{
    assert(byteWidth % 16 == 0);
    ID3D11Buffer* result = nullptr;

    D3D11_BUFFER_DESC Desc;
    Desc.Usage = D3D11_USAGE_DEFAULT;
    Desc.BindFlags = D3D11_BIND_UNORDERED_ACCESS;
    Desc.CPUAccessFlags = 0;
    Desc.MiscFlags = 0;

    Desc.ByteWidth = byteWidth;
    pDevice->CreateBuffer(&Desc, nullptr, &result);
    DXUT_SetDebugName(result, debugName);

    {
        m_VoxelizationStatusRWBuff_UAV = nullptr;
        D3D11_UNORDERED_ACCESS_VIEW_DESC desc;
        desc.Format = DXGI_FORMAT_R32_UINT;
        desc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
        desc.Buffer.FirstElement = 0;
        desc.Buffer.NumElements = byteWidth / 4;
        desc.Buffer.Flags = 0;
        HRESULT hr = pDevice->CreateUnorderedAccessView(result, &desc, &m_VoxelizationStatusRWBuff_UAV );
        DXUT_SetDebugName(m_VoxelizationStatusRWBuff_UAV , debugName);
    }

    return result;
}

ID3D11Buffer* GIManager::GetMeshVoxelizationCB(D3D11_PRIMITIVE_TOPOLOGY topology, CDXUTSDKMesh* pMesh, UINT mesh_idx, UINT subset_idx, ID3D11DeviceContext* pd3dDeviceContext)
{
    assert(mesh_idx < 0xFFFF);
    assert(subset_idx < 0xFFFF);
    UINT key = (mesh_idx << 16) | subset_idx;

    if (m_MeshesCB.find(key) == m_MeshesCB.end())
    {
        ID3D11Device* device = nullptr;
        pd3dDeviceContext->GetDevice(&device);
        char debug_name[128];
        sprintf(&debug_name[0], "MESH_CB_%d[%d]", mesh_idx, subset_idx);
        SDKMESH_SUBSET* pSubset = pMesh->GetSubset(mesh_idx, subset_idx);
        
        ID3D11Buffer* buffer = CreateConstantBuffer(device, sizeof(CB_MESH_DATA), &debug_name[0]);
        D3D11_MAPPED_SUBRESOURCE resource;
        pd3dDeviceContext->Map(buffer, 0, D3D11_MAP_WRITE_NO_OVERWRITE, 0, &resource);
        m_MeshCB_Data = (CB_MESH_DATA*)resource.pData;
        m_MeshCB_Data->index_size = pMesh->GetIndexType(mesh_idx) == IT_16BIT ? 2 : 4;
        m_MeshCB_Data->MV_matrix = DirectX::XMFLOAT4X4(1.0f, 0.0f, 0.0f, 0.0f,
                                                       0.0f, 1.0f, 0.0f, 0.0f,
                                                       0.0f, 0.0f, 1.0f, 0.0f,
                                                       0.0f, 0.0f, 0.0f, 1.0f);
        m_MeshCB_Data->position_offset = 0;
        m_MeshCB_Data->texcoord_offset = 6;
		m_MeshCB_Data->alpha_test_threshold = 0.0f;
		m_MeshCB_Data->tint_color = XMFLOAT3(1, 1, 1);
		m_MeshCB_Data->tint_color2 = XMFLOAT3(1, 1, 1);
		m_MeshCB_Data->index_start = pSubset->IndexStart;
        m_MeshCB_Data->base_vertex = pSubset->VertexStart;
        m_MeshCB_Data->vertex_stride = pMesh->GetVertexStride(mesh_idx, 0);
		m_MeshCB_Data->texCoord_size = 8;
		if (topology == D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST)
		{
			m_MeshCB_Data->triangles_count = pSubset->IndexCount / 3;
			m_MeshCB_Data->primitive_type = 1;
		}
		else if (topology == D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP)
		{
			m_MeshCB_Data->triangles_count = pSubset->IndexCount + 2;
			m_MeshCB_Data->primitive_type = 0;
		}
		else
		{
			assert(false);
		}
        pd3dDeviceContext->Unmap(buffer, 0);

        m_MeshesCB[key] = buffer;
        return buffer;
    }

    return m_MeshesCB[key];
}

ID3D11Texture3D* GIManager::Create3DTexture(ID3D11Device* pDevice, UINT width, UINT height, UINT depth, UINT mips, DXGI_FORMAT format, const char* debugName)
{
    ID3D11Texture3D* result = nullptr;
    D3D11_TEXTURE3D_DESC desc = {};
    desc.Width = width;
    desc.Height = height;
    desc.Depth = depth;
    desc.MipLevels = mips;
    desc.Format = format;// DXGI_FORMAT_R10G10B10A2_UINT;
    desc.Usage = D3D11_USAGE_DEFAULT; //GPU rw
    desc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
    desc.CPUAccessFlags = 0;
    desc.MiscFlags = 0;

    if (mips > 1)
    {
        desc.BindFlags |= D3D11_BIND_RENDER_TARGET;
        desc.MiscFlags |= D3D11_RESOURCE_MISC_GENERATE_MIPS;
    }

    pDevice->CreateTexture3D(&desc, nullptr, &result);
    DXUT_SetDebugName(result, debugName);
    return result;
}

ID3D11Texture2D* GIManager::Create2DTexture(ID3D11Device* pDevice, UINT width, UINT height, DXGI_FORMAT format, const char* debugName)
{
    ID3D11Texture2D* result = nullptr;
    D3D11_TEXTURE2D_DESC desc = {};
    desc.ArraySize = 1;
    desc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
    desc.CPUAccessFlags = 0;
    desc.Format = format;
    desc.Height = height;
    desc.Width = width;
    desc.MipLevels = 1;
    desc.MiscFlags = 0;
    desc.SampleDesc.Quality = 0;
    desc.SampleDesc.Count = 1;
    desc.Usage = D3D11_USAGE_DEFAULT; //GPU rw
    
    pDevice->CreateTexture2D(&desc, nullptr, &result);
    DXUT_SetDebugName(result, debugName);
    return result;
}

ID3D11ShaderResourceView* GIManager::Create3DTextureSRV(ID3D11Device* pDevice, ID3D11Texture3D* texture, int mips, int mostDetailedMip, const char* debugName)
{
    ID3D11ShaderResourceView* view = nullptr;
    D3D11_SHADER_RESOURCE_VIEW_DESC desc;
    desc.Format                 = DXGI_FORMAT_R32_UINT;// DXGI_FORMAT_R10G10B10A2_UINT;//DXGI_FORMAT_R32_TYPELESS;
    desc.ViewDimension          = D3D11_SRV_DIMENSION_TEXTURE3D;
    desc.Texture3D.MipLevels    = mips;
    desc.Texture3D.MostDetailedMip = mostDetailedMip;

    pDevice->CreateShaderResourceView(texture, &desc, &view);
    DXUT_SetDebugName(view, debugName);
    return view;
}

ID3D11UnorderedAccessView* GIManager::Create3DTextureUAV(ID3D11Device* pDevice, ID3D11Texture3D* texture, DXGI_FORMAT format, int mipSlice, const char* debugName)
{
    ID3D11UnorderedAccessView* view = nullptr;
    D3D11_TEXTURE3D_DESC tex_desc;
    texture->GetDesc(&tex_desc);
    D3D11_UNORDERED_ACCESS_VIEW_DESC desc;
    desc.Format                 = format;
    desc.ViewDimension          = D3D11_UAV_DIMENSION_TEXTURE3D;
    desc.Texture3D.MipSlice     = mipSlice;
    desc.Texture3D.FirstWSlice  = 0;
    desc.Texture3D.WSize        = (tex_desc.Depth >> mipSlice);
    HRESULT hr = pDevice->CreateUnorderedAccessView(texture, &desc, &view);
    if (FAILED(hr)) return nullptr;
    DXUT_SetDebugName(view, debugName);
    return view;
}

ID3D11ShaderResourceView* GIManager::Create2DTextureSRV(ID3D11Device* pDevice, ID3D11Texture2D* texture, DXGI_FORMAT format, const char* debugName)
{
    ID3D11ShaderResourceView* view = nullptr;
    D3D11_SHADER_RESOURCE_VIEW_DESC desc;
    desc.Format = format;// DXGI_FORMAT_R10G10B10A2_UINT;//DXGI_FORMAT_R32_TYPELESS;
    desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    desc.Texture2D.MipLevels = 1;
    desc.Texture2D.MostDetailedMip = 0;

    pDevice->CreateShaderResourceView(texture, &desc, &view);
    DXUT_SetDebugName(view, debugName);
    return view;
}

ID3D11UnorderedAccessView* GIManager::Create2DTextureUAV(ID3D11Device* pDevice, ID3D11Texture2D* texture, DXGI_FORMAT format, const char* debugName)
{
    ID3D11UnorderedAccessView* view = nullptr;
    D3D11_TEXTURE2D_DESC tex_desc;
    texture->GetDesc(&tex_desc);

    D3D11_UNORDERED_ACCESS_VIEW_DESC desc;
    desc.Format = format;
    desc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
    desc.Texture2D.MipSlice = 0;

    HRESULT hr = pDevice->CreateUnorderedAccessView(texture, &desc, &view);
    if (FAILED(hr)) return nullptr;
    DXUT_SetDebugName(view, debugName);
    return view;
}

ID3D11ShaderResourceView* GIManager::GetViewForBuffer(ID3D11DeviceContext* pd3dDeviceContext, ID3D11Buffer* buffer, UINT elementsCount, UINT elementSize)
{
    auto it = m_IndicesVerticesViews.find(buffer);
    if (it != m_IndicesVerticesViews.end())
        return it->second;

    ID3D11Device* device = nullptr;
    pd3dDeviceContext->GetDevice(&device);

    D3D11_BUFFER_DESC bdesc;
    buffer->GetDesc(&bdesc);

    D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc = {};
    viewDesc.Format = DXGI_FORMAT_R32_TYPELESS;
    viewDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
    viewDesc.BufferEx.Flags = D3D11_BUFFEREX_SRV_FLAG_RAW;
    UINT size = elementsCount * elementSize;
    viewDesc.BufferEx.NumElements = size / 4;

    ID3D11ShaderResourceView* view = nullptr;
    device->CreateShaderResourceView(buffer, &viewDesc, &view);
    m_IndicesVerticesViews[buffer] = view;
    return view;
}

ID3D11SamplerState* GIManager::CreateSampler(ID3D11Device* pd3dDevice, D3D11_FILTER filter, const char* debug_name)
{
    ID3D11SamplerState* sampler = nullptr;

    D3D11_SAMPLER_DESC SamDescShad =
    {
        filter,// D3D11_FILTER Filter;
        D3D11_TEXTURE_ADDRESS_BORDER, //D3D11_TEXTURE_ADDRESS_MODE AddressU;
        D3D11_TEXTURE_ADDRESS_BORDER, //D3D11_TEXTURE_ADDRESS_MODE AddressV;
        D3D11_TEXTURE_ADDRESS_BORDER, //D3D11_TEXTURE_ADDRESS_MODE AddressW;
        0,//FLOAT MipLODBias;
        0,//UINT MaxAnisotropy;
        D3D11_COMPARISON_LESS , //D3D11_COMPARISON_FUNC ComparisonFunc;
        0.0,0.0,0.0,0.0,//FLOAT BorderColor[ 4 ];
        0,//FLOAT MinLOD;
        0//FLOAT MaxLOD;   
    };

    SamDescShad.MaxAnisotropy = 15;
    SamDescShad.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
    SamDescShad.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
    SamDescShad.Filter = D3D11_FILTER_ANISOTROPIC;
    SamDescShad.ComparisonFunc = D3D11_COMPARISON_NEVER;
    pd3dDevice->CreateSamplerState(&SamDescShad, &sampler);

    if (sampler != nullptr)
    {
        DXUT_SetDebugName(sampler, debug_name);
    }

    return sampler;
}

void GIManager::GetBBox(CDXUTSDKMesh* pMesh, UINT mesh_idx, BBOX& out_bbox)
{
    if (m_BoundingBoxes.empty())
    {
        m_BoundingBoxes.resize(pMesh->GetNumMeshes());
        memset(&m_BoundingBoxes[0], 0, m_BoundingBoxes.size() * sizeof(m_BoundingBoxes[0]));
    }

    if (!m_BoundingBoxes[mesh_idx].initialized)
    {
        float* vertices = (float*)pMesh->GetRawVerticesAt(mesh_idx);
        UINT pos_offset = 0;

        UINT num_vertices = pMesh->GetNumVertices(mesh_idx, 0);
        UINT stride = (pMesh->GetVertexStride(mesh_idx, 0)) >> 2; //bytes count to floats count

        m_BoundingBoxes[mesh_idx].min_point.x = m_BoundingBoxes[mesh_idx].max_point.x = vertices[pos_offset + 0];
        m_BoundingBoxes[mesh_idx].min_point.y = m_BoundingBoxes[mesh_idx].max_point.y = vertices[pos_offset + 1];
        m_BoundingBoxes[mesh_idx].min_point.z = m_BoundingBoxes[mesh_idx].max_point.z = vertices[pos_offset + 2];

        for (UINT i = 1; i < num_vertices; ++i)
        {
            float x = vertices[i * stride + pos_offset + 0];
            float y = vertices[i * stride + pos_offset + 1];
            float z = vertices[i * stride + pos_offset + 2];

            m_BoundingBoxes[mesh_idx].min_point.x = std::min(m_BoundingBoxes[mesh_idx].min_point.x, x);
            m_BoundingBoxes[mesh_idx].min_point.y = std::min(m_BoundingBoxes[mesh_idx].min_point.y, y);
            m_BoundingBoxes[mesh_idx].min_point.z = std::min(m_BoundingBoxes[mesh_idx].min_point.z, z);

            m_BoundingBoxes[mesh_idx].max_point.x = std::max(m_BoundingBoxes[mesh_idx].max_point.x, x);
            m_BoundingBoxes[mesh_idx].max_point.y = std::max(m_BoundingBoxes[mesh_idx].max_point.y, y);
            m_BoundingBoxes[mesh_idx].max_point.z = std::max(m_BoundingBoxes[mesh_idx].max_point.z, z);
        }
    }

    out_bbox = m_BoundingBoxes[mesh_idx];
}

bool GIManager::IsInClipmap(const BBOX& bbox, XMFLOAT3 clipmap_center, XMFLOAT3 clipmap_size)
{
    XMFLOAT3 center((bbox.min_point.x + bbox.max_point.x) * 0.5f,
                    (bbox.min_point.y + bbox.max_point.y) * 0.5f,
                    (bbox.min_point.z + bbox.max_point.z) * 0.5f);
    XMFLOAT3 bbox_half_size((bbox.max_point.x - bbox.min_point.x) * 0.5f,
                            (bbox.max_point.y - bbox.min_point.y) * 0.5f,
                            (bbox.max_point.z - bbox.min_point.z) * 0.5f);

    XMFLOAT3 distance(center.x - clipmap_center.x,
                      center.y - clipmap_center.y,
                      center.z - clipmap_center.z);
    XMFLOAT3 max_distance(bbox_half_size.x + clipmap_size.x * 0.5f,
                          bbox_half_size.y + clipmap_size.y * 0.5f,
                          bbox_half_size.z + clipmap_size.z * 0.5f);

    return  (abs(distance.x) < max_distance.x) &&
        (abs(distance.y) < max_distance.y) &&
        (abs(distance.z) < max_distance.z);
}

//--------------------------------------------------------------------------------------
// Compile and create the CS
//--------------------------------------------------------------------------------------
HRESULT GIManager::CreateComputeShader(LPCWSTR pSrcFile, LPCSTR pFunctionName,
                            ID3D11Device* pDevice, ID3D11ComputeShader** ppShaderOut)
{
    if (!pDevice || !ppShaderOut)
        return E_INVALIDARG;

    // Finds the correct path for the shader file.
    // This is only required for this sample to be run correctly from within the Sample Browser,
    // in your own projects, these lines could be removed safely
    //WCHAR str[MAX_PATH];
    //DXUTFindDXSDKMediaFileCch()
    //HRESULT hr = FindDXSDKShaderFileCch(str, MAX_PATH, pSrcFile);
    //if (FAILED(hr))
    //    return hr;
    LPCWSTR str = pSrcFile;

    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#ifdef _DEBUG
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows 
    // the shaders to be optimized and to run exactly the way they will run in 
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;

    // Disable optimizations to further improve shader debugging
    dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

    const D3D_SHADER_MACRO defines[] =
    {
        "IS_TEST_ENVIRONMENT", "1",

#ifdef USE_STRUCTURED_BUFFERS
        "USE_STRUCTURED_BUFFERS", "1",
#endif

#ifdef TEST_DOUBLE
        "TEST_DOUBLE", "1",
#endif
        nullptr, nullptr
    };

    // We generally prefer to use the higher CS shader profile when possible as CS 5.0 is better performance on 11-class hardware
    LPCSTR pProfile = (pDevice->GetFeatureLevel() >= D3D_FEATURE_LEVEL_11_0) ? "cs_5_0" : "cs_4_0";

    ID3DBlob* pErrorBlob = nullptr;
    ID3DBlob* pBlob = nullptr;

#if D3D_COMPILER_VERSION >= 46

    HRESULT hr = D3DCompileFromFile(str, defines, D3D_COMPILE_STANDARD_FILE_INCLUDE, pFunctionName, pProfile,
                            dwShaderFlags, 0, &pBlob, &pErrorBlob);

#else

    HRESULT hr = D3DX11CompileFromFile(str, defines, nullptr, pFunctionName, pProfile,
                               dwShaderFlags, 0, nullptr, &pBlob, &pErrorBlob, nullptr);

#endif

    if (FAILED(hr))
    {
        if (pErrorBlob)
            OutputDebugStringA((char*)pErrorBlob->GetBufferPointer());

        SAFE_RELEASE(pErrorBlob);
        SAFE_RELEASE(pBlob);

        return hr;
    }

    hr = pDevice->CreateComputeShader(pBlob->GetBufferPointer(), pBlob->GetBufferSize(), nullptr, ppShaderOut);

    SAFE_RELEASE(pErrorBlob);
    SAFE_RELEASE(pBlob);

#if defined(_DEBUG) || defined(PROFILE)
    if (SUCCEEDED(hr))
    {
        (*ppShaderOut)->SetPrivateData(WKPDID_D3DDebugObjectName, lstrlenA(pFunctionName), pFunctionName);
    }
#endif

    return hr;
}

HRESULT GIManager::CreateVSPSShaders(LPCWSTR pSrcFile, LPCSTR pVSFunctionName, LPCSTR pPSFunctionName, ID3D11Device* pDevice, ID3D11VertexShader** ppVSShaderOut, ID3D11PixelShader** ppPSShaderOut)
{
    if (!pDevice || !ppVSShaderOut || !ppPSShaderOut)
        return E_INVALIDARG;

    // Finds the correct path for the shader file.
    // This is only required for this sample to be run correctly from within the Sample Browser,
    // in your own projects, these lines could be removed safely
    //WCHAR str[MAX_PATH];
    //DXUTFindDXSDKMediaFileCch()
    //HRESULT hr = FindDXSDKShaderFileCch(str, MAX_PATH, pSrcFile);
    //if (FAILED(hr))
    //    return hr;
    LPCWSTR str = pSrcFile;

    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#ifdef _DEBUG
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows 
    // the shaders to be optimized and to run exactly the way they will run in 
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;

    // Disable optimizations to further improve shader debugging
    dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

    const D3D_SHADER_MACRO defines[] =
    {
#ifdef USE_STRUCTURED_BUFFERS
        "USE_STRUCTURED_BUFFERS", "1",
#endif

#ifdef TEST_DOUBLE
        "TEST_DOUBLE", "1",
#endif
        nullptr, nullptr
    };

    ID3DBlob* pErrorBlob = nullptr;
    ID3DBlob* pBlob = nullptr;

    assert(m_VertexShaderBlob == nullptr);
    assert(m_PixelShaderBlob == nullptr);

    HRESULT hr = S_OK;

    { //VERTEX SHADER
        hr = D3DCompileFromFile(str, defines, D3D_COMPILE_STANDARD_FILE_INCLUDE, pVSFunctionName, "vs_5_0",
                                        dwShaderFlags, 0, &m_VertexShaderBlob, &pErrorBlob);

        if (FAILED(hr))
        {
            if (pErrorBlob)
                OutputDebugStringA((char*)pErrorBlob->GetBufferPointer());

            SAFE_RELEASE(pErrorBlob);
            SAFE_RELEASE(pBlob);

            return hr;
        }

        hr = pDevice->CreateVertexShader(m_VertexShaderBlob->GetBufferPointer(), m_VertexShaderBlob->GetBufferSize(), nullptr, ppVSShaderOut);
    }
    if (SUCCEEDED(hr))
    { //PIXEL SHADER
        hr = D3DCompileFromFile(str, defines, D3D_COMPILE_STANDARD_FILE_INCLUDE, pPSFunctionName, "ps_5_0",
                                        dwShaderFlags, 0, &m_PixelShaderBlob, &pErrorBlob);

        if (FAILED(hr))
        {
            if (pErrorBlob)
                OutputDebugStringA((char*)pErrorBlob->GetBufferPointer());

            SAFE_RELEASE(pErrorBlob);
            SAFE_RELEASE(pBlob);

            return hr;
        }

        hr = pDevice->CreatePixelShader(m_PixelShaderBlob->GetBufferPointer(), m_PixelShaderBlob->GetBufferSize(), nullptr, ppPSShaderOut);
    }

    SAFE_RELEASE(pErrorBlob);
    SAFE_RELEASE(pBlob);
    SAFE_RELEASE(m_VertexShaderBlob);
    SAFE_RELEASE(m_PixelShaderBlob);

#if defined(_DEBUG) || defined(PROFILE)
    if (SUCCEEDED(hr))
    {
        (*ppVSShaderOut)->SetPrivateData(WKPDID_D3DDebugObjectName, lstrlenA(pVSFunctionName), pVSFunctionName);
        (*ppPSShaderOut)->SetPrivateData(WKPDID_D3DDebugObjectName, lstrlenA(pPSFunctionName), pPSFunctionName);
    }
#endif

    return hr;
}
