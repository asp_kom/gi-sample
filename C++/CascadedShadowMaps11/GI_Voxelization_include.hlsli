#define MAX_NUMBER_OF_VOXEL_CLIPS 50

struct VoxelClipBox
{
    float4 voxel_clip_size;
    float4 voxel_clip_center;
};
