struct VS_INPUT
{
	float3 vPosition        : POSITION;
	float3 vNormal          : NORMAL;
	float2 vTexcoord        : TEXCOORD0;
};

struct VS_OUTPUT
{
	float2 vTexcoord                        : TEXCOORD0;
	float4 vPosition                        : SV_POSITION;
	float4 vWorldPosition					: COLOR0;
};

struct PS_OUTPUT
{
	float4 Target0: SV_TARGET0;
};

//--------------------------------------------------------------------------------------
// Constants
//--------------------------------------------------------------------------------------

cbuffer cameraData : register(b0)
{
	float4x4 VP_Matrix;
	float4x4 VP_Matrix_prev;
	float4x4 ProjMatrix;
	float4x4 InvViewMatrix;
	float4x4 InvProjMatrix;
}

cbuffer interiorBuffer : register(b1)
{
	float4x4 M_Matrix;
	float4x4 M_Matrix_inv;
	float4x4 BBoxRotation;
	float4 bBoxPosition;
	float3 bBoxExtent;
	float pad;
}

//--------------------------------------------------------------------------------------
// Functions
//--------------------------------------------------------------------------------------

struct Box
{
	float4 center;
	float3 extent;
};

struct Ray
{
	float4 start;
	float4 direction;
};

float4 RayPlaneIntersection(Ray ray, float4 plane, out float t)
{
	t = -(dot(plane.xyz, ray.start) + plane.w) / dot(plane.xyz, ray.direction);
	return ray.start + ray.direction * t;
}

bool IntersectRayBox(Ray ray, Box box, out float4 intersectionPoint)
{
	//parallel to XZ
	float3 n = float3(0.0, -1.0, 0.0);
	float t = 0;
	if (dot(ray.direction, n) < -0.001)
	{
		intersectionPoint = RayPlaneIntersection(ray, float4(n, box.center.y + box.extent.y), t);
		if (t > 0 && all(abs(intersectionPoint.xz - box.center.xz) < box.extent.xz)) return true;
	}
	n = float3(0.0, 1.0, 0.0);
	if (dot(ray.direction, n) < -0.001)
	{
		intersectionPoint = RayPlaneIntersection(ray, float4(n, -box.center.y + box.extent.y), t);
		if (t > 0 && all(abs(intersectionPoint.xz - box.center.xz) < box.extent.xz)) return true;
	}
	//parallel to XY
	n = float3(0.0, 0.0, -1.0);
	if (dot(ray.direction, n) < -0.001)
	{
		intersectionPoint = RayPlaneIntersection(ray, float4(n, box.center.z + box.extent.z), t);
		if (t > 0 && all(abs(intersectionPoint.xy - box.center.xy) < box.extent.xy)) return true;
	}
	n = float3(0.0, 0.0, 1.0);
	if (dot(ray.direction, n) < -0.001)
	{
		intersectionPoint = RayPlaneIntersection(ray, float4(n, -box.center.z + box.extent.z), t);
		if (t > 0 && all(abs(intersectionPoint.xy - box.center.xy) < box.extent.xy)) return true;
	}
	//parallel to YZ
	n = float3(-1.0, 0.0, 0.0);
	if (dot(ray.direction, n) < -0.001)
	{
		intersectionPoint = RayPlaneIntersection(ray, float4(n, box.center.x + box.extent.x), t);
		if (t > 0 && all(abs(intersectionPoint.yz - box.center.yz) < box.extent.yz)) return true;
	}
	n = float3(1.0, 0.0, 0.0);
	if (dot(ray.direction, n) < -0.001)
	{
		intersectionPoint = RayPlaneIntersection(ray, float4(n, -box.center.x + box.extent.x), t);
		if (t > 0 && all(abs(intersectionPoint.yz - box.center.yz) < box.extent.yz)) return true;
	}
	return false;
}

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------

VS_OUTPUT VSMain(VS_INPUT Input)
{
	VS_OUTPUT Output;

	matrix mvp_matrix = mul(M_Matrix, VP_Matrix);

	Output.vTexcoord = Input.vTexcoord;
	Output.vPosition = mul(float4(Input.vPosition, 1), mvp_matrix);
	Output.vWorldPosition = mul(float4(Input.vPosition, 1), M_Matrix);
	return Output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------

TextureCube interiorCubeMap : register(t0);
SamplerState samplerLinear    : register(s0);

PS_OUTPUT PSMain(VS_OUTPUT Input)
{
	PS_OUTPUT Output;
	float4 eyePosition = float4(InvViewMatrix._m30, InvViewMatrix._m31, InvViewMatrix._m32, 1);

	float4 eyePositionModelSpace = mul(eyePosition, M_Matrix_inv);
	float4 pointOnPlaneModelSpace = mul(Input.vWorldPosition, M_Matrix_inv);
	eyePositionModelSpace = mul(eyePositionModelSpace, BBoxRotation);
	pointOnPlaneModelSpace = mul(pointOnPlaneModelSpace, BBoxRotation);

	Ray rayFromPointToBBox;
	rayFromPointToBBox.start = pointOnPlaneModelSpace;
	rayFromPointToBBox.direction = normalize(pointOnPlaneModelSpace - eyePositionModelSpace);

	Box cubeMapBox;
	cubeMapBox.center = mul(bBoxPosition, BBoxRotation);
	cubeMapBox.extent = bBoxExtent;
	
	float4 intersectionPoint; 
	if (IntersectRayBox(rayFromPointToBBox, cubeMapBox, intersectionPoint))
	{
		float4 cubeMapDirection = intersectionPoint - cubeMapBox.center;	
		float4 colorFromCubeMap = interiorCubeMap.SampleLevel(samplerLinear, cubeMapDirection, 0);
		Output.Target0 = float4(colorFromCubeMap.rgb, 1);
	}
	else
	{
		//discard;
		Output.Target0 = float4(0, 0, 0, 0);
	}
	return Output;
}