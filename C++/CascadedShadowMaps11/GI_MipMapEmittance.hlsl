

cbuffer MipMapEmittanceCB: register(b0)
{
    uint4   EmittanceHiSize; //w is mip which we want to downscale
    
    //w components not used!
    uint4   EmittanceLoSize;
    float4  EmittanceHiSizeRcp;
    float4  EmittanceLoSizeRcp;
};

Texture3D<uint> t_EmittanceHi: register(t0);
RWTexture3D<uint> u_EmittanceLo: register(u0);

uint collect_samples(int3 location)
{
    uint result = 0;
    for (int x = 0; x < 2; ++x)
    {
        for (int y = 0; y < 2; ++y)
        {
            for (int z = 0; z < 2; ++z)
            {
                result += t_EmittanceHi.Load(int4(location * 2 + int3(x,y,z), 0)).r;
            }
        }
    }

    return result;
}

uint4 gather_samples(int3 location)
{
    int3 packing_offset = int3(EmittanceLoSize.x >> 2, 0, 0);
    uint C = collect_samples(location + packing_offset * int3(3, 1, 1));

    if (C > 0)
    {
        uint R = collect_samples(location);
        uint G = collect_samples(location + packing_offset);
        uint B = collect_samples(location + packing_offset * int3(2, 1, 1));
        return uint4(R, G, B, C) / C;
    }
    else
    {
        return uint4(0, 0, 0, 0);
    }
}

void spread_samples(uint4 samples, int3 location)
{
    int3 packing_offset = int3(EmittanceLoSize.x >> 2, 0, 0);
    u_EmittanceLo[location] = samples.r;
    u_EmittanceLo[location + packing_offset] = samples.g;
    u_EmittanceLo[location + packing_offset * int3(2, 1, 1)] = samples.b;
    u_EmittanceLo[location + packing_offset * int3(3, 1, 1)] = samples.a;
}

[numthreads(4,4,4)]
void CSMain(uint3 dtid: SV_DispatchThreadID)
{
    uint3 location = uint3(dtid.xyz);

    if (any(location.yz > EmittanceLoSize.yz)) return;
    if (location.x > (EmittanceLoSize.x >> 2)) return; //RGBCount are packed along x side

    float3 uvw = float3(location) * EmittanceHiSizeRcp.xyz;
    
    uint4 samples = gather_samples(location);
    spread_samples(samples, location);
}
