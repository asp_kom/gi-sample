

cbuffer MipMapCoverageCB: register(b0)
{
    uint4   CoverageHiSize; //w is mip which we want to downscale
    
    //w components not used!
    uint4   CoverageLoSize;
    float4  CoverageHiSizeRcp;
    float4  CoverageLoSizeRcp;
};

Texture3D<uint> t_CoverageHiPos: register(t0);
Texture3D<uint> t_CoverageHiNeg: register(t1);
RWTexture3D<uint> u_CoverageLoPos: register(u0);
RWTexture3D<uint> u_CoverageLoNeg: register(u1);

uint3 sides_bits_count(uint val)
{
    uint3 result;
    result.x = countbits(val & 0x3FF);
    result.y = countbits(val & 0xFFC00);
    result.z = countbits(val & 0x3FF00000);
    
    return result;
}


/*
    v2      v3
 | 6 7 8 | 6 7 8 |
 | 3 4 5 | 3 4 5 |
 | 0 1 2 | 0 1 2 |
 -----------------
 | 6 7 8 | 6 7 8 |
 | 3 4 5 | 3 4 5 |
 | 0 1 2 | 0 1 2 |
    v0      v1
*/

uint compute_coverage(uint v0, uint v1, uint v2, uint v3)
{
    const uint samples_for_opaque = 2;
    uint    result  =   countbits((v0 & 0x3)    | (v0 & 0x18)) > samples_for_opaque ? 1 : 0;
            result |= ( countbits((v0 & 0x4)    | (v1 & 0x1) | (v0 & 0x20) | (v1 & 0x8)) > samples_for_opaque ? 1 : 0) << 1;
            result |= ( countbits((v1 & 0x30)   | (v1 & 0x180)) > samples_for_opaque ? 1 : 0) << 2;

            result |= ( countbits((v0 & 0xC0)   | (v2 & 0x3)) > samples_for_opaque ? 1 : 0) << 3;
            result |= ( countbits((v0 & 0x100)  | (v1 & 0x40) | (v2 & 0x4) | (v3 & 1)) > samples_for_opaque ? 1 : 0 ) << 4;
            result |= ( countbits((v1 & 0x180)  | (v3 & 0x6)) > samples_for_opaque ? 1 : 0 ) << 5;

            result |= ( countbits((v2 & 0x18)   | (v2 & 0xC0)) > samples_for_opaque ? 1 : 0) << 6;
            result |= ( countbits((v2 & 0x20)   | (v3 & 0x8) | (v2 & 100) | (v3 & 0x40)) > samples_for_opaque ? 1 : 0) << 7;
            result |= ( countbits((v3 & 0x30)   | (v3 & 0x180)) > samples_for_opaque ? 1 : 0) << 8;
    return result;
}

uint2 gather_samples(int3 location)
{
    uint vals[8];
    for (int x_vals = 0; x_vals < 8; ++x_vals)
		vals[x_vals] = 0;
    
    for (int x_pos = 0; x_pos < 2; ++x_pos)
    {
        for (int y_pos = 0; y_pos < 2; ++y_pos)
        {
            for (int z_pos = 0; z_pos < 2; ++z_pos)
            {
                vals[z_pos * 4 + y_pos * 2 + x_pos] += t_CoverageHiPos.Load(int4(location * 2 + int3(x_pos,y_pos,z_pos), 0)).r;
            }
        }
    }
    
    //We have cube consisting of 8 small cubes.
    //We need to compute it's coverage using coverages of small cubes
    uint x = compute_coverage(vals[0 * 4 + 0 * 2 + 1] & 0x3FF, vals[0 * 4 + 1 * 2 + 1] & 0x3FF, vals[1 * 4 + 0 * 2 + 1] * 0x3FF, vals[1 * 4 + 1 * 2 + 1] & 0x3FF);
    uint y = compute_coverage((vals[0 * 4 + 1 * 2 + 0] >> 10) & 0x3FF, (vals[0 * 4 + 1 * 2 + 1] >> 10) & 0x3FF, (vals[1 * 4 + 1 * 2 + 0] >> 10) & 0x3FF, (vals[1 * 4 + 1 * 2 + 1] >> 10) & 0x3FF);
    uint z = compute_coverage((vals[1 * 4 + 0 * 2 + 0] >> 20) & 0x3FF, (vals[1 * 4 + 0 * 2 + 1] >> 20) & 0x3FF, (vals[1 * 4 + 1 * 2 + 0] >> 20) & 0x3FF, (vals[1 * 4 + 1 * 2 + 1] >> 20) & 0x3FF);

    uint2 result;
    result.x = x | (y << 10) | (z << 20);

    for (int x_vals = 0; x_vals < 8; ++x_vals)
		vals[x_vals] = 0;
    for (int x_pos = 0; x_pos < 2; ++x_pos)
    {
        for (int y_pos = 0; y_pos < 2; ++y_pos)
        {
            for (int z_pos = 0; z_pos < 2; ++z_pos)
            {
                vals[z_pos * 4 + y_pos * 2 + x_pos] += t_CoverageHiNeg.Load(int4(location * 2 + int3(x_pos,y_pos,z_pos), 0)).r;
            }
        }
    }

    x = compute_coverage( vals[0 * 4 + 0 * 2 + 0] & 0x3FF, vals[0 * 4 + 1 * 2 + 0] & 0x3FF, vals[1 * 4 + 0 * 2 + 0] * 0x3FF, vals[1 * 4 + 1 * 2 + 0] & 0x3FF);
    y = compute_coverage((vals[0 * 4 + 0 * 2 + 0] >> 10) & 0x3FF, (vals[0 * 4 + 0 * 2 + 1] >> 10) & 0x3FF, (vals[1 * 4 + 0 * 2 + 0] >> 10) & 0x3FF, (vals[1 * 4 + 0 * 2 + 1] >> 10) & 0x3FF);
    z = compute_coverage((vals[0 * 4 + 0 * 2 + 0] >> 20) & 0x3FF, (vals[0 * 4 + 0 * 2 + 1] >> 20) & 0x3FF, (vals[0 * 4 + 1 * 2 + 0] >> 20) & 0x3FF, (vals[0 * 4 + 1 * 2 + 1] >> 20) & 0x3FF);
    result.y = x | (y << 10) | (z << 10);
    return result;
}

[numthreads(4,4,4)]
void CSMain(uint3 dtid: SV_DispatchThreadID)
{
    int3 location = int3(dtid.xyz);

    if (any(location.xyz > CoverageLoSize.xyz)) return;

    uint2 samples = gather_samples(location); //x - pos, y - neg
    u_CoverageLoPos[location] = samples.x;
    u_CoverageLoNeg[location] = samples.y;
}

