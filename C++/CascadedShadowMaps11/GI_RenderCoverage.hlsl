struct VS_INPUT
{
    uint id: SV_VertexID;
};

struct VS_OUTPUT
{
    float2 vTexcoord                        : TEXCOORD0;
    float4 vPosition                        : SV_POSITION;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VSMain( VS_INPUT Input )
{
    VS_OUTPUT Output;

    Output.vTexcoord = float2((Input.id << 1) & 2, Input.id & 2);
    Output.vPosition = float4(Output.vTexcoord * float2(2.0, -2.0) + float2(-1.0, 1.0), 0.0, 1.0);
    return Output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------

#define MAX_NUMBER_OF_VOXEL_CLIPS 10

struct VoxelClipBox
{
    float4 voxel_clip_size;
    float4 voxel_clip_center;
};

cbuffer VoxelizationCB: register(b0)
{
    VoxelClipBox voxelization_area;
    float4 render_offset; // UNUSED
    float2 voxel_size; //size, 1.0 / size
    uint   voxel_grid_resolution;
    uint   VoxelizationCB_pad;
};

cbuffer CameraCB: register(b1)
{
    float4x4 VP_Matrix;
    float4x4 VP_Matrix_prev;
    float4x4 ProjMatrix;
    float4x4 InvViewMatrix;
    float4x4 InvProjMatrix;
};

Texture3D<uint> u_CoveragePos: register(t0);
Texture3D<uint> u_CoverageNeg: register(t1);

struct PS_OUTPUT
{
    float4 Target0: SV_TARGET0;
};

#include "GI_Visualize_include.hlsli"

struct TRACE_RESULTS
{
    float3 emittance;
};

//dir - direction of ray goes "To" voxel
float4 read_coverage(uint3 pos, float3 dir)
{
    uint mask_pos = u_CoveragePos.Load(int4(pos, 0));
    uint mask_neg = u_CoverageNeg.Load(int4(pos, 0));

    float side_samples = 0.0;
    float total_samples = 0.0;
    //mask |= (dir.x < 0.0 ? mask_pos : mask_neg) & 0x3FF;
    //mask |= (dir.y < 0.0 ? mask_pos : mask_neg) & 0xFFC00;
    //mask |= (dir.z < 0.0 ? mask_pos : mask_neg) & 0x3FF00000;

    if (dir.x < 0.1)
    {
        side_samples += float(countbits(mask_pos & 0x3FF));
        total_samples += 9.0;
    }
    if (dir.x > -0.1)
    {
        side_samples += float(countbits(mask_neg & 0x3FF));
        total_samples += 9.0;
    }
    if (dir.y < 0.1)
    {
        side_samples += float(countbits(mask_pos & 0xFFC00));
        total_samples += 9.0;
    }
    if (dir.y > -0.1)
    {
        side_samples += float(countbits(mask_neg & 0xFFC00));
        total_samples += 9.0;
    }
    if (dir.z < 0.1)
    {
        side_samples += float(countbits(mask_pos & 0x3FF00000));
        total_samples += 9.0;
    }
    if (dir.z > -0.1)
    {
        side_samples += float(countbits(mask_neg & 0x3FF00000));
        total_samples += 9.0;
    }


    float result = side_samples / total_samples; //3 sides, 9 bits max on each side
    return float4(result, result, result, side_samples > 0.0 ? 1.0 : 0.0);
}

//float4 read_emittance(uint3 pos)
//{
//    uint3 packing_offset = uint3(voxel_grid_resolution, 0, 0);
//    float r = u_Emittance.Load(int4(pos, 0));
//    float g = u_Emittance.Load(int4(pos + packing_offset, 0));
//    float b = u_Emittance.Load(int4(pos + packing_offset * 2, 0));
//    float3 color_result = float3(r, g, b);
//    float count = u_Emittance.Load(int4(pos + packing_offset * 3, 0));
//    return float4(color_result, count);
//}

TRACE_RESULTS trace(float3 clip_center, float3 clip_size, float3 start, float3 dir)
{
    TRACE_RESULTS result = (TRACE_RESULTS)0;
    //find ray-clip_map intersection
    if (!is_in_clipmap(clip_center, voxelization_area.voxel_clip_size, start))
    {
        float3 intersection = float3(0.0, 0.0, 0.0);
        if (!intersect_clipmap(clip_center, voxelization_area.voxel_clip_size, start, dir, intersection)) return result;
        start = intersection;
    }

    while (is_in_clipmap(clip_center, voxelization_area.voxel_clip_size, start))
    {
        float3 voxel_internal_position;
        uint3 voxel_address = compute_voxel_address(start, dir, voxel_internal_position);
        float4 coverage = read_coverage(voxel_address, dir);
        
        if (coverage.w > 0.01)
        {
            result.emittance = coverage.xyz;
            return result;
        }
        start = raymarch(start, dir);
    }

    return result;
}

PS_OUTPUT PSMain(VS_OUTPUT Input)
{
    PS_OUTPUT Output;
    float3 clip_center = floor(voxelization_area.voxel_clip_center * voxel_size.yyy) * voxel_size.xxx;
    float3 clip_size = ceil((voxelization_area.voxel_clip_size * 0.5f) * voxel_size.yyy) * voxel_size.xxx * 2;

    float2 ndc = screen_to_ndc(Input.vTexcoord);
    float3 pos_in_view = ndc_to_view(float3(ndc, 0.0));
    float3 pos_in_world_near = view_to_world(pos_in_view);
    float3 pos_in_world_far = view_to_world(ndc_to_view(float3(ndc, 0.1)));
    float3 world_direction = pos_in_world_far - pos_in_world_near;
    world_direction = normalize(world_direction);
    TRACE_RESULTS trace_results = trace(clip_center, clip_size, pos_in_world_near, world_direction);

    Output.Target0 = float4(trace_results.emittance, 1.0);

    return Output;
}