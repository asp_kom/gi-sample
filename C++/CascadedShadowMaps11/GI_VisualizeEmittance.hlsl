
struct VS_INPUT
{
    uint id: SV_VertexID;
};

struct VS_OUTPUT
{
    float2 vTexcoord                        : TEXCOORD0;
    float4 vPosition                        : SV_POSITION;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VSMain( VS_INPUT Input )
{
    VS_OUTPUT Output;

    Output.vTexcoord = float2((Input.id << 1) & 2, Input.id & 2);
    Output.vPosition = float4(Output.vTexcoord * float2(2.0, -2.0) + float2(-1.0, 1.0), 0.0, 1.0);
    return Output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------

cbuffer VoxelizationCB: register(b0)
{
    float4 voxel_clip_size; //size, 1.0 / size
    float4 voxel_clip_center;
    float4 voxel_clip_corner;
    float2 voxel_size; //size, 1.0 / size
    uint   voxel_grid_resolution;
    uint   VoxelizationCB_pad;
};

cbuffer CameraCB: register(b1)
{
    float4x4 VP_Matrix;
    float4x4 VP_Matrix_prev;
    float4x4 ProjMatrix;
    float4x4 InvViewMatrix;
    float4x4 InvProjMatrix;
};

Texture3D<uint> u_Emittance: register(t0);

struct PS_OUTPUT
{
    float4 Target0: SV_TARGET0;
};

float2 screen_to_ndc(float2 screen_uv)
{
    screen_uv.y = 1.0 - screen_uv.y;
    return screen_uv * float2(2.0, 2.0) - float2(1.0, 1.0);
}

float3 ndc_to_view(float3 ndc)
{
    float alpha = ProjMatrix._m22;
    float beta = ProjMatrix._m32; //not m32 since matrix is transposed
    
    float3 result = float3(0.0, 0.0, beta / (ndc.z - alpha));
    result.x = ndc.x * result.z / ProjMatrix._m00;
    result.y = ndc.y * result.z / ProjMatrix._m11;
    return result;
}

float3 view_to_world(float3 view)
{
    return mul(float4(view, 1.0), InvViewMatrix).xyz;
}

struct TRACE_RESULTS
{
    float3 emittance;
};

float3 ray_clipmap_intersection(float3 start, float3 dir, float4 plane, out float t)
{
    t = -(dot(plane.xyz, start) + plane.w) / dot(plane.xyz, dir);
    return start + dir * t;
}
/*
bool intersect_clipmap(float3 start, float3 dir, out float3 intersection_point)
{
	float origin_offset = voxel_clip_size.x * 0.5;
	//parallel to XZ
    float3 n = float3(0.0, -1.0, 0.0);
	float t = 0;
    bool result = false;
	if (dot(dir, n) > 0.001)
    {
        intersection_point = ray_clipmap_intersection(start, dir, float4(n, origin_offset), t);
        if (t > 0 && all(abs(intersection_point.xz - voxel_clip_center.xz) < voxel_clip_size.xx * 0.5)) result = true;
    }

    n = float3(0.0, 1.0, 0.0);
    if (dot(dir, n) > 0.001)
    {
        intersection_point = ray_clipmap_intersection(start, dir, float4(n, origin_offset), t);
        if (t > 0 && all(abs(intersection_point.xz - voxel_clip_center.xz) < voxel_clip_size.xx * 0.5)) result = true;
    }

    //parallel to XY
    n = float3(0.0, 0.0, -1.0);
    if (dot(dir, n) > 0.001)
    {
        intersection_point = ray_clipmap_intersection(start, dir, float4(n, origin_offset), t);
        if (t > 0 && all(abs(intersection_point.xy - voxel_clip_center.xy) < voxel_clip_size.xx * 0.5)) result = true;
    }

    n = float3(0.0, 0.0, 1.0);
    if (dot(dir, n) > 0.001)
    {
        intersection_point = ray_clipmap_intersection(start, dir, float4(n, origin_offset), t);
        if (t > 0 && all(abs(intersection_point.xy - voxel_clip_center.xy) < voxel_clip_size.xx * 0.5)) result = true;
    }

    //parallel to YZ
    n = float3(-1.0, 0.0, 0.0);
    if (dot(dir, n) > 0.001)
    {
        intersection_point = ray_clipmap_intersection(start, dir, float4(n, origin_offset), t);
        if (t > 0 && all(abs(intersection_point.yz - voxel_clip_center.yz) < voxel_clip_size.xx * 0.5)) result = true;
    }

    n = float3(1.0, 0.0, 0.0);
    if (dot(dir, n) > 0.001)
    {
        intersection_point = ray_clipmap_intersection(start, dir, float4(n, origin_offset), t);
        if (t > 0 && all(abs(intersection_point.yz - voxel_clip_center.yz) < voxel_clip_size.xx * 0.5)) result = true;
    }

    return result;
}

bool is_in_clipmap(float3 p)
{
    return all(abs(p - voxel_clip_center.xyz) < voxel_clip_size.xxx * 0.5);
}
*/

uint3 compute_voxel_address(float3 v, float3 dir, out float3 voxel_internal_position)
{
    //convert to clip corner space
    v = ((v % (voxel_grid_resolution * voxel_size.x)) + (voxel_grid_resolution * voxel_size.x)) % (voxel_grid_resolution * voxel_size.x);
    //position inside voxel, normalized by voxel size
    voxel_internal_position = (v - floor(v * voxel_size.y) * voxel_size.x) * voxel_size.y;

	float3 temp = v * voxel_size.y;
	if (temp.x - floor(temp.x) < 0.001 || ceil(temp.x) - temp.x < 0.001)
		temp.x += dir.x;
	if (temp.y - floor(temp.y) < 0.001 || ceil(temp.y) - temp.y < 0.001)
		temp.y += dir.y;
	if (temp.z - floor(temp.z) < 0.001 || ceil(temp.z) - temp.z < 0.001)
		temp.z += dir.z;

	uint3 result = temp;
	return result.xyz;
}

float4 read_emittance(uint3 pos)
{
    uint3 packing_offset = uint3(voxel_grid_resolution, 0, 0);
    float r = u_Emittance.Load(int4(pos, 0));
    float g = u_Emittance.Load(int4(pos + packing_offset, 0));
    float b = u_Emittance.Load(int4(pos + packing_offset * 2, 0));
    float3 color_result = float3(r, g, b);
    float count = u_Emittance.Load(int4(pos + packing_offset * 3, 0));
    return float4(color_result, count);
}

float3 raymarch(float3 start, float3 dir)
{
	float3 scaledStart = start * voxel_size.y;
	float3 distances = float3(0.5, 0.5, 0.5);

	if(dir.x > 0)
		distances.x = ceil(scaledStart.x) - scaledStart.x;
	else if (dir.x < 0)
		distances.x = scaledStart.x - floor(scaledStart.x);
	if (dir.y > 0)
		distances.y = ceil(scaledStart.y) - scaledStart.y;
	else if(dir.y < 0)
		distances.y = scaledStart.y - floor(scaledStart.y);
	if (dir.z > 0)
		distances.z = ceil(scaledStart.z) - scaledStart.z;
	else if (dir.z < 0)
		distances.z = scaledStart.z - floor(scaledStart.z);

	float firstMin = min(distances.y, distances.z);
	float firstMax = max(distances.y, distances.z);

	float minDistance = min(distances.x, firstMin);
	float middleDistance = min(distances.x, firstMax);
	float maxDistance = max(distances.x, firstMax);

	float marchScale = 0.5;
	if(minDistance > 0.001)
		marchScale = minDistance;
	else if(middleDistance > 0.001)
		marchScale = middleDistance;
	else if(maxDistance > 0.001)
		marchScale = maxDistance;

	return start + dir * marchScale * voxel_size.x;
}

TRACE_RESULTS trace(float3 clip_center, float3 clip_size, float3 start, float3 dir)
{
    TRACE_RESULTS result = (TRACE_RESULTS)0;
    //find ray-clip_map intersection
    if (!is_in_clipmap(start))
    {
        float3 intersection = float3(0.0, 0.0, 0.0);
        if (!intersect_clipmap(clip_center, clip_size, start, dir, intersection)) return result;
        start = intersection;
    }

    start = raymarch(start, dir);
    while (is_in_clipmap(clip_center, clip_size, start))
    {
        float3 voxel_internal_position;
        uint3 voxel_address = compute_voxel_address(start, dir, voxel_internal_position);
        float4 emittance = read_emittance(voxel_address);
        
        if (emittance.w > 0.01)
        {
            result.emittance = emittance.xyz / emittance.w;
            return result;
        }
        start = raymarch(start, dir);
    }

    return result;
}

PS_OUTPUT PSMain(VS_OUTPUT Input)
{
    PS_OUTPUT Output;
    float3 clip_center = floor(voxel_clip_center * voxel_size.yyy) * voxel_size.xxx;
    float3 clip_size = ceil((voxel_clip_size * 0.5f) * voxel_size.yyy) * voxel_size.xxx * 2;


    float2 ndc = screen_to_ndc(Input.vTexcoord);
    float3 pos_in_view = ndc_to_view(float3(ndc, 0.0));
    float3 pos_in_world_near = view_to_world(pos_in_view);
    float3 pos_in_world_far = view_to_world(ndc_to_view(float3(ndc, 0.1)));
    float3 world_direction = pos_in_world_far - pos_in_world_near;
    world_direction = normalize(world_direction);
    TRACE_RESULTS trace_results = trace(clip_center, clip_size, pos_in_world_near, world_direction);

    Output.Target0 = float4(trace_results.emittance * (1.0 / 256.0), 1.0);

    return Output;
}