//--------------------------------------------------------------------------------------
// File: CascadedShadowManager.h
//
// This is where the shadows are calcaulted and rendered.
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------
#pragma once

#include "ShadowSampleMisc.h"
#include <map>

class CFirstPersonCamera;
class CDXUTSDKMesh;

#pragma warning(push)
#pragma warning(disable: 4324)

struct VOXEL_CLIP_BOX
{
    DirectX::XMFLOAT4   size;
    DirectX::XMFLOAT4   center;
};

struct CB_VOXELIZATION_DATA
{
    VOXEL_CLIP_BOX      voxelization_area;
    DirectX::XMFLOAT4   payload;
    DirectX::XMFLOAT2   voxel_size; //size, 1.0 / size
    UINT                voxel_grid_resolution;
    UINT                pad;
};

struct CB_UPDATE_VOXELIZATION_DATA
{
    VOXEL_CLIP_BOX      voxel_clip_boxes[50];
    UINT                number_clip_boxes;
    UINT                pad;
    UINT                pad1;
    UINT                pad2;
};

struct CB_MESH_DATA
{
    UINT vertex_stride; //size of vertex in sizeof(float)-s
    UINT position_offset; //in sizeof(float)-s
    UINT texcoord_offset; //in sizeof(float)-s
    UINT index_size; //in bytes
    DirectX::XMFLOAT4X4 MV_matrix;
	DirectX::XMFLOAT3 tint_color;
	float alpha_test_threshold;
	DirectX::XMFLOAT3 tint_color2;
	UINT triangles_count;
    UINT index_start;
    UINT base_vertex;
    UINT primitive_type; // 0 for strip, 1 for list
	UINT texCoord_size; //in bytes
};

struct CB_SHADOW_MAP_DATA
{
    DirectX::XMFLOAT4X4 shadow_map_view_projection_matrix;
    DirectX::XMFLOAT4X4 shadow_map_view_projection_matrix_1;
    DirectX::XMFLOAT4X4 shadow_map_view_projection_matrix_2;
    DirectX::XMFLOAT4X4 shadow_map_view_projection_matrix_3;
    UINT num_shadow_maps;
    float shadowed_multiplier;
    UINT pad[2];
};

struct CB_LIGHT_PROPERTIES_DATA
{
    DirectX::XMFLOAT4 light_incoming_direction;
};

struct CB_CAMERA_DATA
{
    DirectX::XMFLOAT4X4 view_projection;
    DirectX::XMFLOAT4X4 view_projection_prev;
    DirectX::XMFLOAT4X4 projection;
    DirectX::XMFLOAT4X4 view_inv;
    DirectX::XMFLOAT4X4 proj_inv;
};

struct CB_INTERIOR_BBOX_DATA
{
	DirectX::XMFLOAT4X4 model_projection;
	DirectX::XMFLOAT4X4 model_projection_inv;
	DirectX::XMFLOAT4X4 BBoxRotation;
	DirectX::XMFLOAT4 position;
	DirectX::XMFLOAT3 extent;
	float pad;
};

struct CB_COMPUTE_DIFFUSE
{
    DirectX::XMUINT2    DiffuseTexSize;
    DirectX::XMFLOAT2   DiffuseTexSizeRCP;
    UINT                DiffuseTexMips;
    float               DiffuseVoxelSize;
    float               DiffuseReprojectionWeight;
    UINT                pad;
};

struct CB_MIPMAP_EMITTANCE
{
    DirectX::XMUINT4   EmittanceHiSize; //w is mip which we want to downscale

    //w components not used!
    DirectX::XMUINT4   EmittanceLoSize;
    DirectX::XMFLOAT4  EmittanceHiSizeRcp;
    DirectX::XMFLOAT4  EmittanceLoSizeRcp;
};

struct CB_MIPMAP_COVERAGE
{
    DirectX::XMUINT4   CoverageHiSize; //w is mip which we want to downscale

    //w components not used!
    DirectX::XMUINT4   CoverageLoSize;
    DirectX::XMFLOAT4  CoverageHiSizeRcp;
    DirectX::XMFLOAT4  CoverageLoSizeRcp;
};

struct BBOX
{
    DirectX::XMFLOAT3 min_point;
    DirectX::XMFLOAT3 max_point;
    bool initialized;
};

struct BoxCenterSize
{
    DirectX::XMFLOAT4 center;
    DirectX::XMFLOAT4 size;
    bool initialized{ false };
};

__declspec(align(16)) class GIManager 
{
public:
    GIManager();
    ~GIManager();
    
    // This runs when the application is initialized.
    HRESULT Init( ID3D11Device* pd3dDevice, CascadeConfig* pCascadeConfig);
    void ComputeGI(ID3D11DeviceContext* pd3dDeviceContext, CDXUTSDKMesh* pMesh, ID3D11ShaderResourceView* pNormalMap, ID3D11ShaderResourceView* pDepthSRV, CFirstPersonCamera* pActiveCamera, float temporal_aa_weight, ID3D11ShaderResourceView* shadowCascade, DirectX::XMMATRIX lightView, DirectX::XMMATRIX lightProj);
    void RenderAlbedo(ID3D11DeviceContext* pd3dDeviceContext, CFirstPersonCamera* pActiveCamera, ID3D11RenderTargetView* prtvBackBuffer, ID3D11DepthStencilView* pdsvBackBuffer);
    void RenderEmittance(ID3D11DeviceContext* pd3dDeviceContext, CFirstPersonCamera* pActiveCamera, ID3D11RenderTargetView* prtvBackBuffer, ID3D11DepthStencilView* pdsvBackBuffer);
    void RenderCoverage(ID3D11DeviceContext* pd3dDeviceContext, CFirstPersonCamera* pActiveCamera, ID3D11RenderTargetView* prtvBackBuffer, ID3D11DepthStencilView* pdsvBackBuffer);
    void RenderDiffuse(ID3D11DeviceContext* pd3dDeviceContext, CFirstPersonCamera* pActiveCamera, ID3D11RenderTargetView* prtvBackBuffer, ID3D11DepthStencilView* pdsvBackBuffer);
	
	void RenderInteriorWindow(ID3D11DeviceContext* pd3dDeviceContext,
		ID3D11RenderTargetView* prtvBackBuffer,
		ID3D11DepthStencilView* pdsvBackBuffer,
		CDXUTSDKMesh* pMesh,
		CFirstPersonCamera* pActiveCamera);

	void SlideVoxelization(DirectX::XMFLOAT3 offset);
	void SlideInteriorBBox(DirectX::XMFLOAT3 offset);
	void ScaleInteriorBBox(DirectX::XMFLOAT3 offset);
	void RotateInteriorBBox(DirectX::XMFLOAT3 offset);
private:
    BBOX perform_voxelization_movement();


    int                     m_CurrentDiffuseTexture;
    ID3D11ComputeShader*    m_CoverageComputeShader;
    ID3D11ComputeShader*    m_CleanupComputeShader;
    ID3D11ComputeShader*    m_ComputeDirectionalLight;
    ID3D11ComputeShader*    m_ComputeDiffuseShader;
    ID3D11ComputeShader*    m_EmittanceMipMapper;
    ID3D11ComputeShader*    m_CoverageMipMapper;
    ID3D11ComputeShader*    m_DiffuseTAA;
    ID3D11VertexShader*     m_RenderEmittanceVSShader;
    ID3D11PixelShader*      m_RenderEmittancePSShader;
    ID3D11VertexShader*     m_RenderCoverageVSShader;
    ID3D11PixelShader*      m_RenderCoveragePSShader;

    ID3D11VertexShader*     m_RenderTextureVSShader;
    ID3D11PixelShader*      m_RenderTexturePSShader;

    ID3DBlob*               m_VertexShaderBlob;
    ID3DBlob*               m_PixelShaderBlob;
    ID3D11Buffer*           m_VoxelizationCB;
    ID3D11Buffer*           m_VoxelizationUpdateCB;
    ID3D11Buffer*           m_VoxelizationStatusRWBuff;
    ID3D11Buffer*           m_lightParamatersBuffer;
    ID3D11Buffer*           m_shadowMapParametersBuffer;
    ID3D11Buffer*           m_CameraBuffer;
    ID3D11Buffer*           m_ComputeDiffuseCB;
    ID3D11Buffer*           m_ComputeEmittanceMipsCB;
    ID3D11Buffer*           m_ComputeCoverageMipsCB;
    ID3D11Texture2D*           m_DiffuseTexture[2];
    ID3D11Texture3D*           m_CoverageTextureXYZ_Pos;
    ID3D11Texture3D*           m_CoverageTextureXYZ_Neg;
    ID3D11Texture3D*           m_AlbedoTexture;
    ID3D11Texture3D*           m_EmittanceTexture;
    ID3D11ShaderResourceView*  m_CoverageTextureXYZ_Pos_SRV;
    ID3D11ShaderResourceView*  m_CoverageTextureXYZ_Neg_SRV;
    ID3D11ShaderResourceView*  m_DiffuseTexture_SRV[2];
    ID3D11UnorderedAccessView*  m_VoxelizationStatusRWBuff_UAV;
    std::vector<ID3D11UnorderedAccessView*> m_EmittanceTexture_UAV;
    std::vector<ID3D11ShaderResourceView*>  m_EmittanceTexture_SRV;
    std::vector<ID3D11UnorderedAccessView*> m_CoverageTexture_UAVPos;
    std::vector<ID3D11UnorderedAccessView*> m_CoverageTexture_UAVNeg;
    std::vector<ID3D11ShaderResourceView*>  m_CoverageTexture_SRVPos;
    std::vector<ID3D11ShaderResourceView*>  m_CoverageTexture_SRVNeg;

    ID3D11ShaderResourceView*   m_EmittanceTextureAll_SRV;
    ID3D11ShaderResourceView*   m_AlbedoTexture_SRV;
    ID3D11UnorderedAccessView*   m_AlbedoTexture_UAV;
    ID3D11UnorderedAccessView*  m_DiffuseTexture_UAV[2];




	ID3D11VertexShader*     m_InteriorMappingCubemapVSShader;
	ID3D11PixelShader*      m_InteriorMappingCubemapPSShader;
	ID3D11Texture2D*			m_CubeMap_Resource;
	ID3D11ShaderResourceView*  m_CubeMap_SRV;
	ID3D11Buffer*           m_InterriorBboxBuffer;
	DirectX::XMFLOAT4			m_InterriorBboxCenter{ -1,0,0,0 };
	DirectX::XMFLOAT3			m_InterriorBboxExtent{ 1,1,1 };
	DirectX::XMFLOAT3			m_InterriorBboxRotation{ 0,0,0 };





    ID3D11SamplerState*         m_SamplerPoint;
    ID3D11SamplerState*         m_SamplerLinear;

    CascadeConfig*              m_pShadowCascadeConfig;

	DirectX::XMFLOAT3			m_Voxelization_Center_Offset{ -1000,-1000,-1000 };
    DirectX::XMFLOAT3           m_Voxelization_Center_Desired{ 0,0,0 };

    CB_UPDATE_VOXELIZATION_DATA*       m_UpdateVoxelizationCB_Data;
    CB_VOXELIZATION_DATA*       m_VoxelizationCB_Data;
    CB_MESH_DATA*               m_MeshCB_Data;

    HRESULT CreateComputeShader(LPCWSTR pSrcFile, LPCSTR pFunctionName,
                                ID3D11Device* pDevice, ID3D11ComputeShader** ppShaderOut);
    HRESULT CreateVSPSShaders(LPCWSTR pSrcFile, LPCSTR pVSFunctionName, LPCSTR pPSFunctionName,
                              ID3D11Device* pDevice, ID3D11VertexShader** ppVSShaderOut, ID3D11PixelShader** ppPSShaderOut);

    ID3D11Buffer* CreateConstantBuffer(ID3D11Device* pDevice, UINT byteWidth, const char* debugName);
    ID3D11Buffer* CreateRWBuffer(ID3D11Device* pDevice, UINT byteWidth, const char* debugName);
    ID3D11Buffer* CreateBuffer(ID3D11Device* pDevice, UINT byteWidth, const char* debugName);
    ID3D11Buffer* GetMeshVoxelizationCB(D3D11_PRIMITIVE_TOPOLOGY topology, CDXUTSDKMesh* pMesh, UINT mesh_idx, UINT subset_idx, ID3D11DeviceContext* pd3dDeviceContext);
    ID3D11Texture3D* Create3DTexture(ID3D11Device* pDevice, UINT width, UINT height, UINT depth, UINT mips, DXGI_FORMAT format, const char* debugName);
    ID3D11Texture2D* Create2DTexture(ID3D11Device* pDevice, UINT width, UINT height, DXGI_FORMAT format, const char* debugName);
    ID3D11ShaderResourceView* Create3DTextureSRV(ID3D11Device* pDevice, ID3D11Texture3D* texture, int mips, int mostDetailedMip, const char* debugName);
    ID3D11UnorderedAccessView* Create3DTextureUAV(ID3D11Device* pDevice, ID3D11Texture3D* texture, DXGI_FORMAT format, int mipSlice, const char* debugName);
    ID3D11ShaderResourceView* Create2DTextureSRV(ID3D11Device* pDevice, ID3D11Texture2D* texture, DXGI_FORMAT format, const char* debugName);
    ID3D11UnorderedAccessView* Create2DTextureUAV(ID3D11Device* pDevice, ID3D11Texture2D* texture, DXGI_FORMAT format, const char* debugName);
    ID3D11ShaderResourceView*   GetViewForBuffer(ID3D11DeviceContext* pd3dDeviceContext, ID3D11Buffer* buffer, UINT elementsInBuffer, UINT elementSize);
    ID3D11SamplerState*         CreateSampler(ID3D11Device* pd3dDevice, D3D11_FILTER filter, const char* debug_name);
    void GetBBox(CDXUTSDKMesh* pMesh, UINT mesh_idx, BBOX& out_bbox);
    bool IsInClipmap(const BBOX& bbox, DirectX::XMFLOAT3 clipmap_center, DirectX::XMFLOAT3 clipmap_size);
    void ComputeDiffuse(ID3D11DeviceContext* pd3dDeviceContext, ID3D11ShaderResourceView* pNormalTexture, ID3D11ShaderResourceView* pDepthSRV, CFirstPersonCamera* pActiveCamera, float weight_temporal_aa);
    void PerformDiffuseTAA(ID3D11DeviceContext* pd3dDeviceContext, ID3D11ShaderResourceView* pDepthSRV);
    void ComputeCoverageEmittance(ID3D11DeviceContext* pd3dDeviceContext, CDXUTSDKMesh* pMesh);
    void ComputeDirectionalLightInfluence(ID3D11DeviceContext* pd3dDeviceContext, ID3D11ShaderResourceView* shadowCascade, DirectX::XMMATRIX lightViewProj);
    void ComputeEmittanceMips(ID3D11DeviceContext* pd3dDeviceContext);
    void ComputeCoverageMips(ID3D11DeviceContext* pd3DeviceContext);
    void CheckDiffuseMap(ID3D11DeviceContext* pd3dDeviceContext);
    HRESULT CreateDiffuseMap(ID3D11DeviceContext* pd3dDeviceContext);

    std::map<ID3D11Buffer*, ID3D11ShaderResourceView*> m_IndicesVerticesViews;
    typedef std::map<ID3D11Buffer*, ID3D11ShaderResourceView*>::value_type views_pair;

    std::vector<BBOX>           m_BoundingBoxes;

    std::map<UINT, ID3D11Buffer*>  m_MeshesCB;
    typedef std::map<UINT, ID3D11Buffer*>::value_type meshescb_pair;

    BoxCenterSize GetBoxOverlap(BoxCenterSize box1, BoxCenterSize box2);
};

#pragma warning(pop)