float2 screen_to_ndc(float2 screen_uv)
{
	screen_uv.y = 1.0 - screen_uv.y;
	return screen_uv * float2(2.0, 2.0) - float2(1.0, 1.0);
}

float3 ndc_to_view(float3 ndc)
{
	float alpha = ProjMatrix._m22;
	float beta = ProjMatrix._m32; //not m32 since matrix is transposed

	float3 result = float3(0.0, 0.0, beta / (ndc.z - alpha));
	result.x = ndc.x * result.z / ProjMatrix._m00;
	result.y = ndc.y * result.z / ProjMatrix._m11;
	return result;
}

float3 view_to_world(float3 view)
{
	return mul(float4(view, 1.0), InvViewMatrix).xyz;
}

float3 ray_clipmap_intersection(float3 start, float3 dir, float4 plane, out float t)
{
	t = -(dot(plane.xyz, start) + plane.w) / dot(plane.xyz, dir);
	return start + dir * t;
}

bool intersect_clipmap(float3 clip_center, float3 clip_size, float3 start, float3 dir, out float3 intersection_point)
{
	float clip_offset = clip_size.x * 0.5;
	//parallel to XZ
	float3 n = float3(0.0, -1.0, 0.0);
	float t = 0;
	if (dot(dir, n) > 0.001)
	{
		intersection_point = ray_clipmap_intersection(start, dir, float4(n, clip_center.y + clip_offset), t);
		if (t > 0 && all(abs(intersection_point.xz - clip_center.xz) < clip_size.xx * 0.5)) return true;
	}

	n = float3(0.0, 1.0, 0.0);
	if (dot(dir, n) > 0.001)
	{
		intersection_point = ray_clipmap_intersection(start, dir, float4(n, -clip_center.y + clip_offset), t);
		if (t > 0 && all(abs(intersection_point.xz - clip_center.xz) < clip_size.xx * 0.5)) return true;
	}

	//parallel to XY
	n = float3(0.0, 0.0, -1.0);
	if (dot(dir, n) > 0.001)
	{
		intersection_point = ray_clipmap_intersection(start, dir, float4(n, clip_center.z + clip_offset), t);
		if (t > 0 && all(abs(intersection_point.xy - clip_center.xy) < clip_size.xx * 0.5)) return true;
	}

	n = float3(0.0, 0.0, 1.0);
	if (dot(dir, n) > 0.001)
	{
		intersection_point = ray_clipmap_intersection(start, dir, float4(n, -clip_center.z + clip_offset), t);
		if (t > 0 && all(abs(intersection_point.xy - clip_center.xy) < clip_size.xx * 0.5)) return true;
	}

	//parallel to YZ
	n = float3(-1.0, 0.0, 0.0);
	if (dot(dir, n) > 0.001)
	{
		intersection_point = ray_clipmap_intersection(start, dir, float4(n, clip_center.x + clip_offset), t);
		if (t > 0 && all(abs(intersection_point.yz - clip_center.yz) < clip_size.xx * 0.5)) return true;
	}

	n = float3(1.0, 0.0, 0.0);
	if (dot(dir, n) > 0.001)
	{
		intersection_point = ray_clipmap_intersection(start, dir, float4(n, -clip_center.x + clip_offset), t);
		if (t > 0 && all(abs(intersection_point.yz - clip_center.yz) < clip_size.xx * 0.5)) return true;
	}

	return false;
}

bool is_in_clipmap(float3 clip_center, float3 clip_size, float3 p)
{
	return all(abs(p - clip_center.xyz) <= clip_size.xxx * 0.501);
}

uint3 compute_voxel_address(float3 v, float3 dir, out float3 voxel_internal_position)
{
	//convert to clip corner space
    //position inside voxel, normalized by voxel size
	voxel_internal_position = (v - floor(v * voxel_size.y) * voxel_size.x) * voxel_size.y;

    /*
    int voxelMaxSize = voxel_grid_resolution;
    int3 voxelAddress = floor((v * voxel_size.yyy));
    voxelAddress = ((voxelAddress % voxelMaxSize) + voxelMaxSize) % voxelMaxSize;
    return voxelAddress;
    */
    
    float voxelMaxSize = voxel_grid_resolution * voxel_size.x;
    v = ((v % voxelMaxSize) + voxelMaxSize) % voxelMaxSize;

    float3 temp = v * voxel_size.yyy;
    if (temp.x - floor(temp.x) < 0.001 || ceil(temp.x) - temp.x < 0.001)
		temp.x += dir.x;
	if (temp.y - floor(temp.y) < 0.001 || ceil(temp.y) - temp.y < 0.001)
		temp.y += dir.y;
	if (temp.z - floor(temp.z) < 0.001 || ceil(temp.z) - temp.z < 0.001)
		temp.z += dir.z;

    return floor(temp) % voxel_grid_resolution;
}

float3 raymarch(float3 start, float3 dir)
{
	float3 scaledStart = start * voxel_size.y;
	float3 distances = float3(0.5, 0.5, 0.5);

	if (dir.x > 0)
		distances.x = ceil(scaledStart.x) - scaledStart.x;
	else if (dir.x < 0)
		distances.x = scaledStart.x - floor(scaledStart.x);
	if (dir.y > 0)
		distances.y = ceil(scaledStart.y) - scaledStart.y;
	else if (dir.y < 0)
		distances.y = scaledStart.y - floor(scaledStart.y);
	if (dir.z > 0)
		distances.z = ceil(scaledStart.z) - scaledStart.z;
	else if (dir.z < 0)
		distances.z = scaledStart.z - floor(scaledStart.z);

	float firstMin = min(distances.y, distances.z);
	float firstMax = max(distances.y, distances.z);

	float minDistance = min(distances.x, firstMin);
	float middleDistance = max(firstMin, min(firstMax, distances.x));
	float maxDistance = max(distances.x, firstMax);

	float marchScale = 0.5;
	if (minDistance > 0.01)
		marchScale = minDistance;
	else if (middleDistance > 0.01)
		marchScale = middleDistance;
	else if (maxDistance > 0.01)
		marchScale = maxDistance;

	return start + dir * marchScale * voxel_size.x;
}