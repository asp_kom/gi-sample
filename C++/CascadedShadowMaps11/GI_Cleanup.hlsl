RWTexture3D<uint> u_CoverageTextureXYZ_Pos: register(u0);
RWTexture3D<uint> u_CoverageTextureXYZ_Neg: register(u1);
RWBuffer<uint> u_VoxelizationStatus: register(u2);
RWTexture3D<uint> u_Emittance: register(u3);

#include "GI_Voxelization_include.hlsli"

cbuffer VoxelizationCB: register(b0)
{
    VoxelClipBox voxelization_area;
    float4 render_offset;
    float2 voxel_size; //size, 1.0 / size
    uint   voxel_grid_resolution;
    uint   pad;
};

cbuffer UpdateVoxelizationCB: register(b1)
{
    VoxelClipBox voxel_clips[MAX_NUMBER_OF_VOXEL_CLIPS];
    uint   numberOfClipBoxes;
}

bool is_within_voxel_clip(uint3 voxel_idx, VoxelClipBox voxelClipBox)
{
    // Calculate Box Center
    float3 boxCenterVoxel = floor(voxelClipBox.voxel_clip_center.xyz * voxel_size.yyy) - 0.1;
    float3 boxCenter = ((boxCenterVoxel % voxel_grid_resolution) + voxel_grid_resolution) % voxel_grid_resolution;

    // Calculate Box Extent
    float3 boxCenterExtent = (voxelClipBox.voxel_clip_size.xyz * 0.5) * voxel_size.yyy;

    // Calculate Distance
    float3 maxValue = max(voxel_idx, boxCenter);
    float3 minValue = min(voxel_idx, boxCenter);

    float3 distanceToUpperEnd = voxel_grid_resolution - maxValue;
    float3 distanceToLowerEnd = minValue;

    float3 distanceInSpace = maxValue - minValue;
    return all((distanceInSpace < boxCenterExtent) || (distanceToLowerEnd + distanceToUpperEnd < boxCenterExtent));
}

bool is_within_area(uint3 voxel_idx)
{ 
    for (uint i = 0; i < numberOfClipBoxes; i++)
    {
        if (is_within_voxel_clip(voxel_idx, voxel_clips[i])) return true;
    }
    return false;
}

[numthreads(32,1,1)]
void CSMain(uint3 dtid: SV_DispatchThreadID)
{
    uint3 grid_size;
    u_CoverageTextureXYZ_Pos.GetDimensions(grid_size.x, grid_size.y, grid_size.z);

    uint3 voxel_idx;
    voxel_idx.z = dtid.x / (grid_size.x * grid_size.y);
    voxel_idx.y = (dtid.x % (grid_size.x * grid_size.y)) / grid_size.x;
    voxel_idx.x = (dtid.x % (grid_size.x * grid_size.y)) % grid_size.x;

    if (is_within_area(voxel_idx))
    {
        u_CoverageTextureXYZ_Pos[voxel_idx] = 0;
        u_CoverageTextureXYZ_Neg[voxel_idx] = 0;
        u_Emittance[voxel_idx] = 0;
        u_Emittance[voxel_idx + uint3(grid_size.x, 0, 0)] = 0;
        u_Emittance[voxel_idx + uint3(grid_size.x * 2, 0, 0)] = 0;
        u_Emittance[voxel_idx + uint3(grid_size.x * 3, 0, 0)] = 0;
        u_VoxelizationStatus[dtid.x % 1024] = 0;
    }
}
