
struct VS_INPUT
{
    uint id: SV_VertexID;
};

struct VS_OUTPUT
{
    float2 vTexcoord                        : TEXCOORD0;
    float4 vPosition                        : SV_POSITION;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VSMain( VS_INPUT Input )
{
    VS_OUTPUT Output;

    Output.vTexcoord = float2((Input.id << 1) & 2, Input.id & 2);
    Output.vPosition = float4(Output.vTexcoord * float2(2.0, -2.0) + float2(-1.0, 1.0), 0.0, 1.0);
    return Output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------

Texture2D u_Texture: register(t0);

SamplerState g_SamLinear: register( s0 );

struct PS_OUTPUT
{
    float4 Target0: SV_TARGET0;
};

PS_OUTPUT PSMain(VS_OUTPUT Input)
{
    uint2 dimensions;
    u_Texture.GetDimensions(dimensions.x, dimensions.y);

    PS_OUTPUT Output;
    float4 result = u_Texture.SampleLevel(g_SamLinear, Input.vTexcoord, 0);
    Output.Target0 = result;
    return Output;
}