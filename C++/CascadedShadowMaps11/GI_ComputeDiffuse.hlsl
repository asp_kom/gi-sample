cbuffer ComputeDiffuseCB: register(b0)
{
    uint2   DiffuseTexSize;
    float2  DiffuseTexSizeRCP;
    uint    EmittanceTexMips;
    float   DiffuseVoxelSize;
    float   DiffuseReprojectionWeight;
    uint    Diffuse_pad;
};

cbuffer LightPropertiesCB: register(b1)
{
    float4 light_incoming_direction;
};

#include "GI_Voxelization_include.hlsli"

cbuffer VoxelizationCB: register(b2)
{
    VoxelClipBox voxelization_area;
    float4 render_offset;
    float2 voxel_size; //size, 1.0 / size
    uint   voxel_grid_resolution;
    uint   numberOfClipBoxes;
};

cbuffer CameraCB: register(b3)
{
    float4x4 VP_Matrix;
    float4x4 VP_Matrix_prev;
    float4x4 ProjMatrix;
    float4x4 InvViewMatrix;
    float4x4 InvProjMatrix;
};

RWTexture2D<uint> u_Diffuse: register(u0);

Texture2D t_Normal: register(t0);
Texture2D t_Depth: register(t1);
Texture3D<uint> u_Emittance: register(t2);
Texture2D t_Diffuse_prev: register(t3);

SamplerState g_SamLinear: register( s0 );

float3 rotate(float3 from, float3 to, float factor)
{
    float3 buf = lerp(from, to, factor);
    return normalize(buf);
}

void get_tangent_bitangent(float3 normal, out float3 tangent, out float3 bitangent)
{
   float3 abs_n = abs(normal);
   float maxComp = max(abs_n.x, max(abs_n.y, abs_n.z));
   if (maxComp == abs_n.x)
    tangent = float3((-normal.y - normal.z) * sign(normal.x), abs_n.x, abs_n.x);
   else if (maxComp == abs_n.y)
    tangent = float3(abs_n.y, (-normal.x - normal.z) * sign(normal.y), abs_n.y);
   else
    tangent = float3(abs_n.z, abs_n.z, (-normal.x - normal.y) * sign(normal.z));

   tangent = normalize(tangent);
   bitangent = cross(tangent, normal);
}

float luma(float3 rgb)
{
    float3 l = float3(0.2126, 0.7152, 0.0722);
    return dot(l, rgb);
}

float compute_level_from_distance(float distance, float tan_angle_by_2)
{
    float level = max(log2(2.0 * distance * tan_angle_by_2 * voxel_size.y), 0.0);
    return min(level, float(EmittanceTexMips));
}

uint3 compute_voxel_address_for_clip(float voxel_size_rcp, float3 v, float levelModifier)
{
    int voxelMaxSize = voxel_grid_resolution / levelModifier;
    int3 voxelAddress = floor((v * voxel_size_rcp));
    voxelAddress = ((voxelAddress % voxelMaxSize) + voxelMaxSize) % voxelMaxSize;
    return voxelAddress;
}

void compute_voxel_address(float3 v, float distance, float tan_angle_by_2, out uint3 addr_lo_lvl, out uint3 addr_hi_lvl, out float lvl)
{
    lvl = compute_level_from_distance(distance, tan_angle_by_2);

    uint lvl_lo = floor(lvl);
    uint lvl_hi = ceil(lvl);

    float temp_lo = float(exp2(lvl_lo));
    float temp_hi = float(exp2(lvl_hi));

    float voxel_size_rcp_lo = voxel_size.y / temp_lo;
    float voxel_size_rcp_hi = voxel_size.y / temp_hi;

    addr_lo_lvl = compute_voxel_address_for_clip(voxel_size_rcp_lo, v, temp_lo);
    addr_hi_lvl = compute_voxel_address_for_clip(voxel_size_rcp_hi, v, temp_hi);
}

float4 read_emittance(uint3 pos, uint level)
{
    uint level_resolution = voxel_grid_resolution >> level;
    uint3 packing_offset = uint3(level_resolution, 0, 0);
    float r = u_Emittance.Load(int4(pos, level));
    float g = u_Emittance.Load(int4(pos + packing_offset, level));
    float b = u_Emittance.Load(int4(pos + packing_offset * 2, level));
    //float r = u_Emittance.SampleLevel(g_SamLinear, float3(pos.xyz) * voxel_size.y, level).r;
    //float g = u_Emittance.SampleLevel(g_SamLinear, float3(pos.xyz + packing_offset) * voxel_size.y, level).r;
    //float b = u_Emittance.SampleLevel(g_SamLinear, float3(pos.xyz + packing_offset * 2) * voxel_size.y, level).r;

    float3 color_result = float3(r, g, b) * (1.0 / 256.0);
    float count = u_Emittance.Load(int4(pos + packing_offset * 3, level));
    return float4(color_result, count);
}

bool is_in_clipmap(float3 p)
{
    return all(abs(p - voxelization_area.voxel_clip_center.xyz) < voxelization_area.voxel_clip_size.xyz * 0.5);
}


#define TRACE_CONE_STEPS 6
float3 trace_cone(float3 n, float tan_angle_by_2, float3 intialPos)
{
    float angle = tan_angle_by_2; //just shorthand

    float3 result = float3(0.0, 0.0, 0.0);
    float distance  = 0.0;
    float distance_step = DiffuseVoxelSize;

    distance = 2.0 * DiffuseVoxelSize;

    float3 pos = intialPos + n * distance;

    for(int i=0; i< EmittanceTexMips; i++)
    {
        if (is_in_clipmap(pos))
        {
            uint3 voxel_lo, voxel_hi;
            float lvl;
            compute_voxel_address(pos, distance, angle, voxel_lo, voxel_hi, lvl);
            float attenuation = 1;//exp(-distance); //

            float4 emittance = read_emittance(voxel_lo, floor(lvl));

            float3 additionalResult = float3(0, 0, 0);
            if (emittance.w > 0.1)
            {
                additionalResult += ((attenuation * emittance.rgb) / emittance.w) * (lvl - floor(lvl));
            }
            emittance = read_emittance(voxel_hi, ceil(lvl));
            if (emittance.w > 0.1)
            {
                additionalResult += ((attenuation * emittance.rgb) / emittance.w) * (1.0 - (lvl - floor(lvl)));
            }

            result += additionalResult * (1 - length(result.rgb));

            if (length(result) > 1)
                break;
        }
        distance_step = angle * distance;
        distance += distance_step;
        pos = intialPos + n * distance;
    }

    return result;
}

float2 screen_to_ndc(float2 screen_uv)
{
    screen_uv.y = 1.0 - screen_uv.y;
    return screen_uv * float2(2.0, 2.0) - float2(1.0, 1.0);
}

float3 ndc_to_view(float3 ndc, float4x4 proj_matrix)
{
    float alpha = proj_matrix._m22;
    float beta = proj_matrix._m32; //not m32 since matrix is transposed
    
    float3 result = float3(0.0, 0.0, beta / (ndc.z - alpha));
    result.x = ndc.x * result.z / proj_matrix._m00;
    result.y = ndc.y * result.z / proj_matrix._m11;
    return result;
}

float3 view_to_world(float3 view, float4x4 inv_view_matrix)
{
    return mul(float4(view, 1.0), inv_view_matrix).xyz;
}

float3 reconstruct_position(int2 screen_pos, float4x4 proj_matrix, float4x4 inv_view_matrix)
{
    float d = t_Depth.Load(int3(screen_pos, 0)).r;
    float3 ndc = float3(screen_to_ndc(float2(screen_pos)* DiffuseTexSizeRCP), d);
    float3 view = ndc_to_view(ndc, proj_matrix);
    //return view;
    float3 world = view_to_world(view, inv_view_matrix);
    return world + render_offset.xyz;
}

float4 find_prev_result(float3 world_pos, float4x4 vp_matrix_prev)
{
    float4 clip_pos = mul(float4(world_pos, 1.0), vp_matrix_prev);
    float2 ndc = clip_pos.xy / clip_pos.w;
    float2 screen_uv = ndc * 0.5 + 0.5;
    screen_uv.y = 1.0 - screen_uv.y;

    float3 result = t_Diffuse_prev.Load(int3(screen_uv * float2(DiffuseTexSize), 0)).xyz;
    return float4(result, luma(result));
}

#ifndef D3DX11INLINE
//taken from d3dx_dxgiformatconvert.inl
#define D3DX11INLINE
#define hlsl_precise precise

#define D3DX_Saturate_FLOAT(_V) saturate(_V)

typedef int INT;
typedef uint UINT;

typedef float2 XMFLOAT2;
typedef float3 XMFLOAT3;
typedef float4 XMFLOAT4;
typedef int2 XMINT2;
typedef int4 XMINT4;
typedef uint2 XMUINT2;
typedef uint4 XMUINT4;

D3DX11INLINE UINT D3DX_FLOAT_to_UINT(FLOAT _V,
                                     FLOAT _Scale)
{
    return (UINT)floor(_V * _Scale + 0.5f);
}


//-----------------------------------------------------------------------------
// R8G8B8A8_UINT <-> UINT4
//-----------------------------------------------------------------------------
 XMUINT4 D3DX_R8G8B8A8_UINT_to_UINT4(UINT packedInput)
{
    XMUINT4 unpackedOutput;
    unpackedOutput.x =  packedInput      & 0x000000ff;
    unpackedOutput.y = (packedInput>> 8) & 0x000000ff;
    unpackedOutput.z = (packedInput>>16) & 0x000000ff;
    unpackedOutput.w =  packedInput>>24;
    return unpackedOutput;
}

D3DX11INLINE UINT D3DX_UINT4_to_R8G8B8A8_UINT(XMUINT4 unpackedInput)
{
    UINT packedOutput;
    unpackedInput.x = min(unpackedInput.x, 0x000000ff);
    unpackedInput.y = min(unpackedInput.y, 0x000000ff);
    unpackedInput.z = min(unpackedInput.z, 0x000000ff);
    unpackedInput.w = min(unpackedInput.w, 0x000000ff);
    packedOutput = ( unpackedInput.x      |
                    (unpackedInput.y<< 8) |
                    (unpackedInput.z<<16) |
                    (unpackedInput.w<<24) );
    return packedOutput;
}

//-----------------------------------------------------------------------------
// R8G8B8A8_UNORM <-> FLOAT4
//-----------------------------------------------------------------------------
D3DX11INLINE XMFLOAT4 D3DX_R8G8B8A8_UNORM_to_FLOAT4(UINT packedInput)
{
    hlsl_precise XMFLOAT4 unpackedOutput;
    unpackedOutput.x = (FLOAT)(packedInput & 0x000000ff) / 255;
    unpackedOutput.y = (FLOAT)(((packedInput >> 8) & 0x000000ff)) / 255;
    unpackedOutput.z = (FLOAT)(((packedInput >> 16) & 0x000000ff)) / 255;
    unpackedOutput.w = (FLOAT)(packedInput >> 24) / 255;
    return unpackedOutput;
}

D3DX11INLINE UINT D3DX_FLOAT4_to_R8G8B8A8_UNORM(hlsl_precise XMFLOAT4 unpackedInput)
{
    UINT packedOutput;
    packedOutput = ((D3DX_FLOAT_to_UINT(D3DX_Saturate_FLOAT(unpackedInput.x), 255)) |
        (D3DX_FLOAT_to_UINT(D3DX_Saturate_FLOAT(unpackedInput.y), 255) << 8) |
        (D3DX_FLOAT_to_UINT(D3DX_Saturate_FLOAT(unpackedInput.z), 255) << 16) |
        (D3DX_FLOAT_to_UINT(D3DX_Saturate_FLOAT(unpackedInput.w), 255) << 24));
    return packedOutput;
}

#endif

float3 vec_half_sphere_decode(float2 half_sphere_vec_in)
{
    float3 vec_out;

    //*** BEGIN BLOCK CODE ***
    float3 temp_vec = float3(half_sphere_vec_in.r, half_sphere_vec_in.g, sqrt(1.0 - saturate(dot(half_sphere_vec_in, half_sphere_vec_in))));
    temp_vec = (2.0 * temp_vec.z) * temp_vec;

    vec_out = float3(0.0, 0.0, 1.0) + float3(temp_vec.x, temp_vec.y, -temp_vec.z);
    //*** END BLOCK CODE ***

    return vec_out;
}

float2 tex_s1616_decode(float4 encoded_value)
{
    float2 signed_value;

    //*** BEGIN BLOCK CODE ***
#if defined(_XBOX)
    signed_value = encoded_value.rg;
#elif defined(_PS3)
    signed_value = (encoded_value.rg * 2.0) - 1.0;
#else 
    signed_value = (encoded_value.rg * 2.0) - 1.0;
#endif
    //*** END BLOCK CODE ***

    return signed_value;
}

[numthreads(8, 8, 1)]
void CSMain(uint3 dtid: SV_DispatchThreadID)
{
    int2 location = int2(dtid.xy);
    if (any(location > int2(DiffuseTexSize)))
    {
        return;
    }
    //Test code
    //u_Diffuse[location] = float4(location, 0.0, 1.0) * float4(DiffuseTexSizeRCP, 1.0, 1.0);
    //u_Diffuse[location] = float4(t_Normal.Load(int3(location, 0)).xyz, 1.0);

    //7 cones that cover half sphere (1 along normal, 6 along combinations of t,bt,n vectors),
    //all cover angle of pi/3)
    float4 coded_normal = t_Normal.Load(int3(location, 0)); //normal
    float3 n = vec_half_sphere_decode(tex_s1616_decode(coded_normal));
    float3 t, bt; //tangent, bitangent
    get_tangent_bitangent(n, t, bt);
    float angle = 1.047197551197; // ~ pi/3
    float rotate_angle_param_0 = 1.0 / 3.0; //parameter for lerping with normal
    float rotate_angle_param_1 = 2.0 / 3.0; //parameter for lerping with bitangent
    float tan_angle_by_2 = 0.5773503; //tan(angle * 0.5)
    float3 pos = reconstruct_position(location, ProjMatrix, InvViewMatrix);
    /*
    if (!is_in_clipmap(pos))
    {
        u_Diffuse[location] = 0;
        return;
    }
    */

    //Debug code
    //u_Diffuse[location] = float4(pos * 0.01, t_Depth.Load(int3(location, 0)).r);
    //return;

    float3 diffuse = trace_cone(n, tan_angle_by_2, pos);
    
    diffuse += trace_cone(rotate( t, n, rotate_angle_param_0), tan_angle_by_2, pos);
    diffuse += trace_cone(rotate(-t, n, rotate_angle_param_0), tan_angle_by_2, pos);

    float3 buf = rotate(t, bt, rotate_angle_param_1);
    diffuse += trace_cone(rotate(buf, n, rotate_angle_param_0),   tan_angle_by_2, pos);
    diffuse += trace_cone(rotate(-buf, n, rotate_angle_param_0),  tan_angle_by_2, pos);

    buf = rotate(-t, bt, rotate_angle_param_1);
    diffuse += trace_cone(rotate(buf, n, rotate_angle_param_0),   tan_angle_by_2, pos);
    diffuse += trace_cone(rotate(-buf, n, rotate_angle_param_0),  tan_angle_by_2, pos);

    diffuse *= (1.0 / 7.0); //normalize
    //diffuse *= (1.0 / 3.14159265);

    float4 result = float4(diffuse, luma(diffuse));
    u_Diffuse[location] = D3DX_FLOAT4_to_R8G8B8A8_UNORM(result);
}

