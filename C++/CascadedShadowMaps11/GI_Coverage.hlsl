#define TRIANGLE_STRIP 0
#define TRIANGLE_LIST 1

#include "GI_Voxelization_include.hlsli"

cbuffer VoxelizationCB: register(b0)
{
    VoxelClipBox voxelization_area;
    float4 render_offset;
    float2 voxel_size; //size, 1.0 / size
    uint   voxel_grid_resolution;
    uint   pad;
};

cbuffer MeshCB: register(b1)
{
    uint vertex_stride; //size of vertex in sizeof(float)-s
    uint position_offset; //in sizeof(float)-s
    uint texcoord_offset; //in sizeof(float)-s
    uint index_size; //in bytes
    float4x4 MV_matrix;
    float3 tint_color;
    float alpha_test_threshold;
    float3 tint_color2;
    uint triangles_count;
    uint index_start;
    uint base_vertex;
    uint primitive_type;
    uint texCoord_size;
};

cbuffer UpdateVoxelizationCB: register(b2)
{
    VoxelClipBox voxel_clips[MAX_NUMBER_OF_VOXEL_CLIPS];
    uint   numberOfClipBoxes;
}

RWTexture3D<uint> u_CoverageTextureXYZ_Pos: register(u0);
RWTexture3D<uint> u_CoverageTextureXYZ_Neg: register(u1);
RWBuffer<uint> u_VoxelizationStatus: register(u2);
RWTexture3D<uint> u_Emittance: register(u3);

ByteAddressBuffer Indices : register(t0);
ByteAddressBuffer Vertices : register(t1);

Texture2D t_DiffuseTexture: register(t2);

float3 sample_diffuse_level(float2 uv, uint mip)
{
    uint samples_count = 1 << mip;
    float4 result = float4(0.0, 0.0, 0.0, 0.0);
    uint3 dimensions;
    t_DiffuseTexture.GetDimensions(0, dimensions.x, dimensions.y, dimensions.z);

    uv.x = frac(abs(1 + uv.x));
    uv.y = frac(abs(1 + uv.y));

    int2 xy = floor(uv * float2(dimensions.xy)) - int2(samples_count >> 1, samples_count >> 1);
    for (uint j = 0; j < samples_count; ++j)
    {
        xy.x = floor(uv.x * float(dimensions.x)) - (samples_count >> 1);
        for (uint i = 0; i < samples_count; ++i)
        {
            result += t_DiffuseTexture.Load(int3(xy % dimensions.xy, 0));
            xy += int2(1, 0);
        }
        xy += int2(0, 1);
    }
    result /= float(samples_count * samples_count);

    // use green chanel to lerp between two colors as in "v_sample_two_tone_diffuse"
    if (alpha_test_threshold < 0)
        result = float4(result.ggg * lerp(tint_color, tint_color2, result.a), result.a);
    else if (result.a < alpha_test_threshold)
        result.xyz = float3(0, 0, 0);
    return result.xyz;
}

float2 compute_uv(float3 v0, float3 v1, float3 v2, float3 v, float2 uv0, float2 uv1, float2 uv2)
{
    float3 n = cross(v1 - v0, v2 - v0);
    float area = length(n) * 0.5;
    float area_v12 = length(cross(v1 - v, v2 - v)) * 0.5;
    float area_v01 = length(cross(v1 - v, v0 - v)) * 0.5;
    float area_v20 = length(cross(v2 - v, v0 - v)) * 0.5;

    float u0 = area_v12 / area;
    float u1 = area_v20 / area;
    float u2 = 1.0 - u0 - u1;

    return uv0 * u0 + uv1 * u1 + uv2 * u2;
}

int compute_mip_level(float3 v0, float3 v1, float3 v2, float2 uv0, float2 uv1, float2 uv2, float voxel_size)
{
    return 0;
}

float3 sample_point_in_triangle(float3 v0, float3 v1, float3 v2, float dx, float dy, float ix, float iy)
{
    return v0 + (v1 - v0) * ix * dx + (v2 - v0) * iy * dy;
}

void load_indices(uint tri_idx, out uint idx0, out uint idx1, out uint idx2)
{
    idx0 = 0;
    idx1 = 0;
    idx2 = 0;
    if (primitive_type == TRIANGLE_LIST)
    {
        if (index_size == 4)
        {
            idx0 = Indices.Load((tri_idx * 3 + 0) * 4);
            idx1 = Indices.Load((tri_idx * 3 + 1) * 4);
            idx2 = Indices.Load((tri_idx * 3 + 2) * 4);
        }
        else //index_size == 2
        {
            uint idx_buf_0 = Indices.Load(((tri_idx * 3) >> 1) * 4);
            uint idx_buf_1 = Indices.Load((((tri_idx * 3) >> 1) + 1) * 4);
            if ((tri_idx & 1) == 0)
            {
                idx0 = idx_buf_0 & 0xFFFF;
                idx1 = idx_buf_0 >> 16;
                idx2 = idx_buf_1 & 0xFFFF;
            }
            else
            {
                idx0 = idx_buf_0 >> 16;
                idx1 = idx_buf_1 & 0xFFFF;
                idx2 = idx_buf_1 >> 16;
            }
        }
    }
    else if (primitive_type == TRIANGLE_STRIP)
    {
        if (index_size == 4)
        {
            if ((tri_idx & 1) == 0)
            {
                idx0 = Indices.Load((tri_idx + 0) * 4);
                idx1 = Indices.Load((tri_idx + 1) * 4);
                idx2 = Indices.Load((tri_idx + 2) * 4);
            }
            else
            {
                idx0 = Indices.Load((tri_idx + 0) * 4);
                idx1 = Indices.Load((tri_idx + 1) * 4);
                idx2 = Indices.Load((tri_idx + 2) * 4);
            }
        }
        else
        {
            uint idx_buf_0 = Indices.Load((tri_idx >> 1) * 4);
            uint idx_buf_1 = Indices.Load(((tri_idx >> 1) + 1) * 4);
            if ((tri_idx & 1) == 0)
            {
                idx0 = idx_buf_0 & 0xFFFF;
                idx1 = idx_buf_0 >> 16;
                idx2 = idx_buf_1 & 0xFFFF;
            }
            else
            {
                idx0 = idx_buf_0 >> 16;
                idx1 = idx_buf_1 & 0xFFFF;
                idx2 = idx_buf_1 >> 16;
            }
        }
    }
}

float convertShortUV(uint uvInShortPart)
{
    uint x_sign = (uvInShortPart & (1 << 15)) << 16;
    float other = float((uvInShortPart & 0x7FFF) | x_sign);
    return other / 1024.0f;
}

void load_verticies(uint idx0, uint idx1, uint idx2, out float3 v0, out float3 v1, out float3 v2)
{
    v0.x = asfloat(Vertices.Load((idx0 * vertex_stride) + (position_offset + 0) * 4));
    v0.y = asfloat(Vertices.Load((idx0 * vertex_stride) + (position_offset + 1) * 4));
    v0.z = asfloat(Vertices.Load((idx0 * vertex_stride) + (position_offset + 2) * 4));

    v1.x = asfloat(Vertices.Load((idx1 * vertex_stride) + (position_offset + 0) * 4));
    v1.y = asfloat(Vertices.Load((idx1 * vertex_stride) + (position_offset + 1) * 4));
    v1.z = asfloat(Vertices.Load((idx1 * vertex_stride) + (position_offset + 2) * 4));

    v2.x = asfloat(Vertices.Load((idx2 * vertex_stride) + (position_offset + 0) * 4));
    v2.y = asfloat(Vertices.Load((idx2 * vertex_stride) + (position_offset + 1) * 4));
    v2.z = asfloat(Vertices.Load((idx2 * vertex_stride) + (position_offset + 2) * 4));
}

void load_texcoords(uint idx0, uint idx1, uint idx2, out float2 uv0, out float2 uv1, out float2 uv2)
{
    if (texCoord_size == 4)
    {
        uint loadedValue = Vertices.Load((idx0 * vertex_stride) + (texcoord_offset + 0) * 4);
        uv0.x = convertShortUV(loadedValue & 0xFFFF);
        uv0.y = convertShortUV(loadedValue >> 16);

        loadedValue = Vertices.Load((idx1 * vertex_stride) + (texcoord_offset + 0) * 4);
        uv1.x = convertShortUV(loadedValue & 0xFFFF);
        uv1.y = convertShortUV(loadedValue >> 16);

        loadedValue = Vertices.Load((idx2 * vertex_stride) + (texcoord_offset + 0) * 4);
        uv2.x = convertShortUV(loadedValue & 0xFFFF);
        uv2.y = convertShortUV(loadedValue >> 16);
    }
    else
    {
        uv0.x = asfloat(Vertices.Load((idx0 * vertex_stride) + (texcoord_offset + 0) * 4));
        uv0.y = asfloat(Vertices.Load((idx0 * vertex_stride) + (texcoord_offset + 1) * 4));

        uv1.x = asfloat(Vertices.Load((idx1 * vertex_stride) + (texcoord_offset + 0) * 4));
        uv1.y = asfloat(Vertices.Load((idx1 * vertex_stride) + (texcoord_offset + 1) * 4));

        uv2.x = asfloat(Vertices.Load((idx2 * vertex_stride) + (texcoord_offset + 0) * 4));
        uv2.y = asfloat(Vertices.Load((idx2 * vertex_stride) + (texcoord_offset + 1) * 4));
    }
}

uint3 compute_voxel_address(float3 v, out float3 voxel_internal_position)
{
    //position inside voxel, normalized by voxel size
    float3 positionInVoxelSpace = v * voxel_size.y;
    voxel_internal_position = positionInVoxelSpace - floor(positionInVoxelSpace);

    int voxelMaxSize = voxel_grid_resolution;
    int3 voxelAddress = floor(positionInVoxelSpace);
    voxelAddress = ((voxelAddress % voxelMaxSize) + voxelMaxSize) % voxelMaxSize;
    return voxelAddress;
}

uint compute_bit_address(float2 v)
{
    uint result = 1 << ((v.x > 0.33 ? 1 : 0) + (v.x > 0.66 ? 1 : 0));
    result = result << ((v.y > 0.33 ? 3 : 0) + (v.y > 0.66 ? 3 : 0));
    return result;
}

uint3 compute_voxel_internal_address(float3 voxel_internal_position)
{
    uint3 result;
    //x=const plane | variables: yz
    result.x = compute_bit_address(voxel_internal_position.yz);

    //y=const plane | variables: xz
    result.y = compute_bit_address(voxel_internal_position.xz);

    //z=const plane | variables: xy
    result.z = compute_bit_address(voxel_internal_position.xy);
    return result;
}

float min_of_3(float a, float b, float c)
{
    return min(a, min(b, c));
}

float max_of_3(float a, float b, float c)
{
    return max(a, max(b, c));
}

struct BoxData
{
    float3 center;
    float3 size;
};

bool is_point_inside_clip_box(float3 v, BoxData boxData)
{
    float3 d = abs(v - boxData.center);
    return all(d <= boxData.size * 0.5);
}

bool triangle_intersects_clip_box(float3 v0, float3 v1, float3 v2, BoxData boxData)
{
    float3 min_p, max_p;
    min_p.x = min_of_3(v0.x, v1.x, v2.x);
    min_p.y = min_of_3(v0.y, v1.y, v2.y);
    min_p.z = min_of_3(v0.z, v1.z, v2.z);

    max_p.x = max_of_3(v0.x, v1.x, v2.x);
    max_p.y = max_of_3(v0.y, v1.y, v2.y);
    max_p.z = max_of_3(v0.z, v1.z, v2.z);

    float3 center = (min_p + max_p) * 0.5;
    float3 distanceBetweenCenters = abs(boxData.center - center);

    return all(distanceBetweenCenters <= boxData.size * 0.5 + (max_p - center));
}

struct TriangleData
{
    //Precomputed data for calculating barycentric coordinates
    float d00;
    float d01;
    float d11;
    float invDenom;

    //Vertices
    float3 v[3];
};

void populate_triangle_data(float3 v0, float3 v1, float3 v2, out TriangleData tri)
{
    tri.v[0] = v0;
    tri.v[1] = v1;
    tri.v[2] = v2;

    float3 temp1 = v1 - v0;
    float3 temp2 = v2 - v0;

    tri.d00 = dot(temp1, temp1);
    tri.d01 = dot(temp1, temp2);
    tri.d11 = dot(temp2, temp2);

    float denominator = (tri.d00 * tri.d11 - tri.d01 * tri.d01);
    if (denominator > 0)
    {
        tri.invDenom = 1.0 / (tri.d00 * tri.d11 - tri.d01 * tri.d01);
    }
}

bool get_inner_triangle(TriangleData tri, float distance, out TriangleData toReturn)
{
    float3 center = (tri.v[0] + tri.v[1] + tri.v[2]) / 3;

    float3 inner[3];
    float v0_center_distance = length(center - tri.v[0]);
    float v1_center_distance = length(center - tri.v[1]);
    float v2_center_distance = length(center - tri.v[2]);

    if (v0_center_distance > distance && v1_center_distance > distance && v2_center_distance > distance)
    {
        inner[0] = tri.v[0] + normalize(center - tri.v[0]) * distance;
        inner[1] = tri.v[1] + normalize(center - tri.v[1]) * distance;
        inner[2] = tri.v[2] + normalize(center - tri.v[2]) * distance;
        populate_triangle_data(inner[0], inner[1], inner[2], toReturn);
        return true;
    }
    else
    {
        toReturn = tri; 
        return false;   
    }
}

float3 compute_barycentric(float3 p, TriangleData tri)
{
    float3 barycentric;
    float3 v0 = tri.v[1] - tri.v[0];
    float3 v1 = tri.v[2] - tri.v[0];
    float3 v2 = p - tri.v[0];
    float d20 = dot(v2, v0);
    float d21 = dot(v2, v1);
    barycentric.y = (tri.d11 * d20 - tri.d01 * d21) * tri.invDenom;
    barycentric.z = (tri.d00 * d21 - tri.d01 * d20) * tri.invDenom;
    barycentric.x = 1.0f - barycentric.y - barycentric.z;
    return barycentric;
}

bool barycentric_outside_triangle(float3 barycentric)
{
    return any(barycentric < 0 || barycentric > 1);
}

void voxelize(float3 v0, float3 v1, float3 v2, float2 uv0, float2 uv1, float2 uv2, uint mip_level, uint plane_mask, uint3 dtid, float precise_step_size, float coarse_step_size, BoxData clipBoxes[MAX_NUMBER_OF_VOXEL_CLIPS])
{
    TriangleData tri;
    populate_triangle_data(v0, v1, v2, tri);

    TriangleData innerTri;
    bool triangle_is_big_enough = get_inner_triangle(tri, coarse_step_size, innerTri);

    float step_size = precise_step_size;
    float3 v01 = v1 - v0;
    float3 v02 = v2 - v0;
    float3 v12 = v2 - v1;

    float v01_len = min(length(v01), voxel_size.x * voxel_grid_resolution);
    float v02_len = min(length(v02), voxel_size.x * voxel_grid_resolution);
    float v12_len = min(length(v12), voxel_size.x * voxel_grid_resolution);

    //float step_size = voxel_size.x * 0.1; //since we use r10g10b10a2_uint format => 1 color channel of texel = 3x3 bitfield, 1 redundant

    float v01_len_rcp = rcp(v01_len);
    float v12_len_rcp = rcp(v12_len);

    float2 sizes = float2(v01_len, v02_len);
    float2 cur_pos = float2(0.0, 0.0);
    float cos_v0v1v2 = dot(v12, -v01) * v01_len_rcp * v12_len_rcp;

    v01 = normalize(v01);
    v02 = normalize(v02);
    uint counter = 0;

    while (cur_pos.x < sizes.x)
    {
        cur_pos.y = 0.0;
        while (cur_pos.y < sizes.y)
        {
            counter = counter + 1;
            if (counter > 4000) //optimize
            {
                //uint result_code = 0x80000000 | (dtid.x / 1024);
                //u_VoxelizationStatus[dtid.x % 1024] = result_code;
                return;
            }

            step_size = precise_step_size;
            bool isStepPrecise = false;

            float3 v = v0 + cur_pos.x * v01 + cur_pos.y * v02;

            //check if in triangle
            {
                float cos_v0v1v = dot(v - v1, -v01) / length(v - v1);
                if (cos_v0v1v < cos_v0v1v2) break;
            }
            //check if in voxel clip
            {
                bool is_inside_clip_boxes = false;
                for (uint i = 0; i < numberOfClipBoxes; i++)
                {
                    if (is_point_inside_clip_box(v, clipBoxes[i]))
                    {
                        is_inside_clip_boxes = true;
                        break;
                    }
                }

                if(!is_inside_clip_boxes)
                {
                    cur_pos.y += step_size;
                    continue;
                }
            }
            /*
            // check if near border of triangle
            {
                if (triangle_is_big_enough && !barycentric_outside_triangle(compute_barycentric(v, innerTri)))
                {
                    step_size = coarse_step_size;
                    isStepPrecise = false;
                }
            }
            */
            //ok, we are in triangle
            //propagate sample volume to voxels
            {
                float3 voxel_internal_location;
                uint3  voxel_address = compute_voxel_address(v, voxel_internal_location);

                //{
                //    //Debug code. To check if 3D Write textures are bound properly
                //    uint3 addr0 = uint3(dtid.x % 256, (dtid.x / 256 + dtid.x % 16) % 16, 0);
                //    uint val = asuint(v.x) + asuint(v.y) + asuint(v.z);
                //    InterlockedOr(u_CoverageTextureXYZ_Pos[addr0], val);
                //    InterlockedOr(u_CoverageTextureXYZ_Neg[addr0], val);
                //    cur_pos.y += step_size;
                //    continue;
                //}

                //Test code
                // Populate Emitance Texture
                // Populate Emittance only for voxels directly lit by directional light
                {
                    uint3 voxel_packing_offset = uint3(voxel_grid_resolution, 0, 0);
                    float2 uv = compute_uv(v0, v1, v2, v, uv0, uv1, uv2);
                    float3 diff = sample_diffuse_level(uv, mip_level);

                    if (length(diff) > 0.01)
                    {
                        uint diff_ui = uint(diff.r * 255.0f);
                        InterlockedAdd(u_Emittance[voxel_address], diff_ui);

                        diff_ui = uint(diff.g * 255.0f);
                        InterlockedAdd(u_Emittance[voxel_address + voxel_packing_offset], diff_ui);

                        diff_ui = uint(diff.b * 255.0f);
                        InterlockedAdd(u_Emittance[voxel_address + voxel_packing_offset * 2], diff_ui);

                        InterlockedAdd(u_Emittance[voxel_address + voxel_packing_offset * 3], 1);
                    }
                }

                /*
                // Populate Coverage Texture
                {
                    uint neg_val = 0;
                    uint pos_val = 0;
                    uint3 voxel_internal_address = uint3(0x1FF, 0x1FF, 0x1FF);
                    if (isStepPrecise)
                    {
                        voxel_internal_address = compute_voxel_internal_address(voxel_internal_location);
                    }
                    //positive
                    if (plane_mask & 0x7)
                    {
                        //R +x
                        pos_val |= voxel_internal_address.x * (plane_mask & 1);
                        //G +y
                        pos_val |= (voxel_internal_address.y * ((plane_mask >> 1) & 1)) << 10;
                        //B +z
                        pos_val |= (voxel_internal_address.z * ((plane_mask >> 2) & 1)) << 20;
                    }
                    //negative
                    if (plane_mask & 0x38)
                    {
                        //R -x
                        neg_val |= voxel_internal_address.x * ((plane_mask >> 3) & 1);
                        //G -y
                        neg_val |= (voxel_internal_address.y * ((plane_mask >> 4) & 1)) << 10;
                        //B -z
                        neg_val |= (voxel_internal_address.z * ((plane_mask >> 5) & 1)) << 20;
                    }

                    InterlockedOr(u_CoverageTextureXYZ_Pos[voxel_address], pos_val);
                    InterlockedOr(u_CoverageTextureXYZ_Neg[voxel_address], neg_val);
                }
                */
            }
            cur_pos.y += step_size;
        }
        cur_pos.x += step_size;
    }

    //u_VoxelizationStatus[dtid.x % 1024] = 0;
}

[numthreads(32, 1, 1)]
void CSMain(uint3 dtid: SV_DispatchThreadID)
{
    if (triangles_count <= dtid.x) return;

    BoxData clipBoxes[MAX_NUMBER_OF_VOXEL_CLIPS];
    for (uint i = 0; i < numberOfClipBoxes; i++)
    {
        clipBoxes[i].center = (floor(voxel_clips[i].voxel_clip_center.xyz * voxel_size.yyy)) * voxel_size.xxx;
        clipBoxes[i].size = voxel_clips[i].voxel_clip_size.xyz;
    }

    uint triangle_index = dtid.x + index_start / 3;
    if (primitive_type == TRIANGLE_STRIP)
    {
        triangle_index = dtid.x + index_start;
    }

    uint idx0, idx1, idx2;
    idx0 = 0;
    idx1 = 0;
    idx2 = 0;
    load_indices(triangle_index, idx0, idx1, idx2);
    if (idx0 == idx1 || idx0 == idx2 || idx1 == idx2) return;
    idx0 += base_vertex;
    idx1 += base_vertex;
    idx2 += base_vertex;

    float3 v0, v1, v2; //in world space
    load_verticies(idx0, idx1, idx2, v0, v1, v2);
    v0 = mul(float4(v0.xyz, 1.0), MV_matrix).xyz;
    v1 = mul(float4(v1.xyz, 1.0), MV_matrix).xyz;
    v2 = mul(float4(v2.xyz, 1.0), MV_matrix).xyz;

    bool intersects_any_clip_box = false;
    for (uint j = 0;j < numberOfClipBoxes; j++)
    {
        if (triangle_intersects_clip_box(v0, v1, v2, clipBoxes[j]))
        {
            intersects_any_clip_box = true;
            break;
        }
    }
    if (!intersects_any_clip_box) return;

    float3 v01 = v1 - v0;
    float3 v02 = v2 - v0;
    float3 v12 = v2 - v1;

    float v01_len = length(v01);
    float v02_len = length(v02);
    float v12_len = length(v12);

    if ((v01_len < 0.01) ||
        (v02_len < 0.01) ||
        (v12_len < 0.01)) return;

    float3 normal = cross(v01 / v01_len, v02 / v02_len);

    float2 uv0, uv1, uv2;
    load_texcoords(idx0, idx1, idx2, uv0, uv1, uv2);

#if 0
    {
        //Debug code. To check if 3D Write textures are bound properly
        uint3 addr0 = uint3(dtid.x % 256, (dtid.x / 256 + dtid.x % 16) % 16, 0);
        uint val = asuint(v1.x) + asuint(v1.y) + asuint(v1.z);
        val += asuint(v2.x) + asuint(v2.y) + asuint(v2.z);
        val += asuint(v0.x) + asuint(v0.y) + asuint(v0.z);
        InterlockedOr(u_CoverageTextureXYZ_Pos[addr0], val);
        InterlockedOr(u_CoverageTextureXYZ_Neg[addr0], val);
        return;
    }
#endif

    //Each voxel consist of 2 texels in 2 r10g10b10a2_uint textures
    //Each color channel means 1 voxel side plane
    //Check which planes triangle is projected on
    uint plane_mask = 0;
    //positive planes
    plane_mask |= normal.x > -0.1f ? 1 << 0 : 0;
    plane_mask |= normal.y > -0.1f ? 1 << 1 : 0;
    plane_mask |= normal.z > -0.1f ? 1 << 2 : 0;
    //negative planes
    plane_mask |= normal.x < 0.1f ? 1 << 3 : 0;
    plane_mask |= normal.y < 0.1f ? 1 << 4 : 0;
    plane_mask |= normal.z < 0.1f ? 1 << 5 : 0;

    uint mip_level = compute_mip_level(v0, v1, v2, uv0, uv1, uv2, voxel_size.x);

    voxelize(v0, v1, v2, uv0, uv1, uv2, mip_level, plane_mask, dtid, voxel_size.x / 3, voxel_size.x, clipBoxes);
}