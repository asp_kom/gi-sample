

cbuffer ComputeDiffuseCB: register(b0)
{
    uint2   DiffuseTexSize;
    float2  DiffuseTexSizeRCP;
    uint    EmittanceTexMips;
    float   DiffuseVoxelSize;
    float   DiffuseReprojectionWeight;
    uint    Diffuse_pad;
};

cbuffer CameraCB: register(b3)
{
    float4x4 VP_Matrix;
    float4x4 VP_Matrix_prev;
    float4x4 ProjMatrix;
    float4x4 InvViewMatrix;
    float4x4 InvProjMatrix;
};



RWTexture2D<uint> u_Diffuse_prev: register(u0);
Texture2D t_Diffuse: register(t0);
Texture2D t_Depth: register(t1);



float2 screen_to_ndc(float2 screen_uv)
{
    screen_uv.y = 1.0 - screen_uv.y;
    return screen_uv * float2(2.0, 2.0) - float2(1.0, 1.0);
}

float3 ndc_to_view(float3 ndc, float4x4 proj_matrix)
{
    float alpha = proj_matrix._m22;
    float beta = proj_matrix._m32; //not m32 since matrix is transposed
    
    float3 result = float3(0.0, 0.0, beta / (ndc.z - alpha));
    result.x = ndc.x * result.z / proj_matrix._m00;
    result.y = ndc.y * result.z / proj_matrix._m11;
    return result;
}

float3 view_to_world(float3 view, float4x4 inv_view_matrix)
{
    return mul(float4(view, 1.0), inv_view_matrix).xyz;
}

float3 reconstruct_position(int2 screen_pos, float4x4 proj_matrix, float4x4 inv_view_matrix)
{
    float d = t_Depth.Load(int3(screen_pos, 0)).r;
    float3 ndc = float3(screen_to_ndc(float2(screen_pos)* DiffuseTexSizeRCP), d);
    float3 view = ndc_to_view(ndc, proj_matrix);
    //return view;
    float3 world = view_to_world(view, inv_view_matrix);
    return world;
}


float luma(float3 rgb)
{
    float3 l = float3(0.2126, 0.7152, 0.0722);
    return dot(l, rgb);
}


#ifndef D3DX11INLINE
//taken from d3dx_dxgiformatconvert.inl
#define D3DX11INLINE
#define hlsl_precise precise

#define D3DX_Saturate_FLOAT(_V) saturate(_V)

typedef int INT;
typedef uint UINT;

typedef float2 XMFLOAT2;
typedef float3 XMFLOAT3;
typedef float4 XMFLOAT4;
typedef int2 XMINT2;
typedef int4 XMINT4;
typedef uint2 XMUINT2;
typedef uint4 XMUINT4;

D3DX11INLINE UINT D3DX_FLOAT_to_UINT(FLOAT _V,
                                     FLOAT _Scale)
{
    return (UINT)floor(_V * _Scale + 0.5f);
}


//-----------------------------------------------------------------------------
// R8G8B8A8_UINT <-> UINT4
//-----------------------------------------------------------------------------
 XMUINT4 D3DX_R8G8B8A8_UINT_to_UINT4(UINT packedInput)
{
    XMUINT4 unpackedOutput;
    unpackedOutput.x =  packedInput      & 0x000000ff;
    unpackedOutput.y = (packedInput>> 8) & 0x000000ff;
    unpackedOutput.z = (packedInput>>16) & 0x000000ff;
    unpackedOutput.w =  packedInput>>24;
    return unpackedOutput;
}

D3DX11INLINE UINT D3DX_UINT4_to_R8G8B8A8_UINT(XMUINT4 unpackedInput)
{
    UINT packedOutput;
    unpackedInput.x = min(unpackedInput.x, 0x000000ff);
    unpackedInput.y = min(unpackedInput.y, 0x000000ff);
    unpackedInput.z = min(unpackedInput.z, 0x000000ff);
    unpackedInput.w = min(unpackedInput.w, 0x000000ff);
    packedOutput = ( unpackedInput.x      |
                    (unpackedInput.y<< 8) |
                    (unpackedInput.z<<16) |
                    (unpackedInput.w<<24) );
    return packedOutput;
}

//-----------------------------------------------------------------------------
// R8G8B8A8_UNORM <-> FLOAT4
//-----------------------------------------------------------------------------
D3DX11INLINE XMFLOAT4 D3DX_R8G8B8A8_UNORM_to_FLOAT4(UINT packedInput)
{
    hlsl_precise XMFLOAT4 unpackedOutput;
    unpackedOutput.x = (FLOAT)  (packedInput      & 0x000000ff)  / 255;
    unpackedOutput.y = (FLOAT)(((packedInput>> 8) & 0x000000ff)) / 255;
    unpackedOutput.z = (FLOAT)(((packedInput>>16) & 0x000000ff)) / 255;
    unpackedOutput.w = (FLOAT)  (packedInput>>24)                / 255;
    return unpackedOutput;
}

D3DX11INLINE UINT D3DX_FLOAT4_to_R8G8B8A8_UNORM(hlsl_precise XMFLOAT4 unpackedInput)
{
    UINT packedOutput;
    packedOutput = ( (D3DX_FLOAT_to_UINT(D3DX_Saturate_FLOAT(unpackedInput.x), 255))     |
                     (D3DX_FLOAT_to_UINT(D3DX_Saturate_FLOAT(unpackedInput.y), 255)<< 8) |
                     (D3DX_FLOAT_to_UINT(D3DX_Saturate_FLOAT(unpackedInput.z), 255)<<16) |
                     (D3DX_FLOAT_to_UINT(D3DX_Saturate_FLOAT(unpackedInput.w), 255)<<24) );
    return packedOutput;
}

#endif

float4 find_prev_result(float3 world_pos, float4x4 vp_matrix_prev)
{
    float4 clip_pos = mul(float4(world_pos, 1.0), vp_matrix_prev);
    float2 ndc = clip_pos.xy / clip_pos.w;
    float2 screen_uv = ndc * 0.5 + 0.5;
    screen_uv.y = 1.0 - screen_uv.y;

    float3 result = D3DX_R8G8B8A8_UNORM_to_FLOAT4( u_Diffuse_prev.Load(int3(screen_uv * float2(DiffuseTexSize), 0)).r ).xyz;
    return float4(result, luma(result));
}

void get_aabb(int2 location, out float3 min_p, out float3 max_p)
{
    min_p = max_p = t_Diffuse.Load(int3(location, 0)).xyz;

    if (location.x > 0)
    {
        if (location.y > 0)
        {
            min_p = min(t_Diffuse.Load(int3(location + int2(-1, -1), 0)).xyz, min_p);
            max_p = max(t_Diffuse.Load(int3(location + int2(-1, -1), 0)).xyz, max_p);
        }
        
        min_p = min(t_Diffuse.Load(int3(location + int2(-1, 0), 0)).xyz, min_p);
        max_p = max(t_Diffuse.Load(int3(location + int2(-1, 0), 0)).xyz, max_p);

        if (location.y < DiffuseTexSize.y)
        {
            min_p = min(t_Diffuse.Load(int3(location + int2(-1, 1), 0)).xyz, min_p);
            max_p = max(t_Diffuse.Load(int3(location + int2(-1, 1), 0)).xyz, max_p);
        }
    }

    if (location.x < DiffuseTexSize.x)
    {
        if (location.y < DiffuseTexSize.y)
        {
            min_p = min(t_Diffuse.Load(int3(location + int2(1, 1), 0)).xyz, min_p);
            max_p = max(t_Diffuse.Load(int3(location + int2(1, 1), 0)).xyz, max_p);
        }
        min_p = min(t_Diffuse.Load(int3(location + int2(1, 0), 0)).xyz, min_p);
        max_p = max(t_Diffuse.Load(int3(location + int2(1, 0), 0)).xyz, max_p);
    }

    if (location.y > 0)
    {
        if (location.x < DiffuseTexSize.x)
        {
            min_p = min(t_Diffuse.Load(int3(location + int2(1, -1), 0)).xyz, min_p);
            max_p = max(t_Diffuse.Load(int3(location + int2(1, -1), 0)).xyz, max_p);
        }
    }

    if (location.y < DiffuseTexSize.y)
    {
        min_p = min(t_Diffuse.Load(int3(location + int2(0, 1), 0)).xyz, min_p);
        max_p = max(t_Diffuse.Load(int3(location + int2(0, 1), 0)).xyz, max_p);
    }

    if (location.y > 0)
    {
        min_p = min(t_Diffuse.Load(int3(location + int2(0, -1), 0)).xyz, min_p);
        max_p = max(t_Diffuse.Load(int3(location + int2(0, -1), 0)).xyz, max_p);
    }
}

[numthreads(8,8,1)]
void CSMain(uint3 dtid: SV_DispatchThreadID)
{
    int2 location = int2(dtid.xy);
    if (any(location > DiffuseTexSize)) return;

    

    float4 result = t_Diffuse.Load(int3(location, 0));

    if (DiffuseReprojectionWeight > 0.0001)
    {
        float3 pos = reconstruct_position(location, ProjMatrix, InvViewMatrix);
        float4 prev_result = find_prev_result(pos, VP_Matrix_prev);
        float3 aabb_min, aabb_max;
        get_aabb(location, aabb_min, aabb_max);
        prev_result.xyz = clamp(prev_result.xyz, aabb_min, aabb_max);
        result = lerp(result, prev_result, DiffuseReprojectionWeight);
    }
    u_Diffuse_prev[location] = D3DX_FLOAT4_to_R8G8B8A8_UNORM(result);
}

